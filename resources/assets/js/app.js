
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

window.justifedlayout = require('justified-layout')
import PushSubscriptionManager from './managers/PushSubscriptionManager'

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/service-worker.js');
}

$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

var justifylayout = function() {
    $('.justified-layout').each(function() {
        var ratios = []
        $(this).children().each(function() {
            ratios.push(parseFloat($(this).find('img').data('aspectratio')))
        })
        var layout = justifedlayout(ratios, {
            containerWidth: $(this).width(),
            containerPadding: 0,
            boxSpacing: 5,
            targetRowHeight: 200
        })
        $(this).height(layout.containerHeight)
        $(this).children().each(function(index) {
            $(this).css({
                top: layout.boxes[index].top,
                left: layout.boxes[index].left,
                width: layout.boxes[index].width,
                height: layout.boxes[index].height
            })
        })
    })
}

var initFolderSelect = function() {
    let input = $('<input type="hidden" name="folders" />')
    $('.folder-select').append(input)
    $('.folder-select > *').click(function() {
        $(this).toggleClass('selected')
        refreshFolderSelect($(this).parent())
    })
}

let refreshFolderSelect = function(select) {
    let selected = []
    $(select).children().each(function() {
        $(this).hasClass('selected') ? selected.push($(this).data('id')) : null
    })
    $(select).children('input[name=folders]').prop('value', selected.join(','))
}

let initPictureInput = function() {
    $('.picture-input').click(function(event) {
        if($(event.target).is('input[type=file]')) {
            return true
        }
        $(this).find('input[type=file]').click()
    })

    $('.picture-input > input[type=file]').on('change paste keyup', function() {
        $(this).val().length > 0 ? $(this).parent().addClass('done') && $(this).parent().find('.mi-Camera').removeClass('mi-Camera').addClass('mi-Accept') : $(this).parent().removeClass('done') && $(this).parent().find('.mi-Accept').removeClass('mi-Accept').addClass('mi-Camera')
    })
}

let initMenuScroll = function() {
    var lastposition = window.scrollY;
    $(window).scroll($.throttle(250, function() {
        var $menu = $('#page-header');
        var difference = window.scrollY - lastposition;
        if(difference < 0) {
            $menu.removeClass('hide')
        } else {
            $menu.addClass('hide')
        }
        if(window.scrollY < 1) {
            $menu.removeClass('reduced')
        } else {
            $menu.addClass('reduced')
        }
        lastposition = window.scrollY;
    }))
}

let initGrowing = function() {
    autosize($('textarea.growing'))
}

let initUsernameLive = function() {
    $('.username-live input[type=username]').on('change input', function() {
        $.post('/validate-username', { username: this.value }).done(function(data) {
            if(data.valid) {
                $('.username-live .username-route-live').text(decodeURIComponent(data.url))
                $('.username-live .username-invalid').hide()
                $('.username-live .username-valid').show()
                $('.username-live [type=submit]').removeClass('button-disabled').prop('disabled', '')
            } else {
                $('.username-live .username-errors').html(data.errors.map(error => '<li>' + error + '</li>').join(''))
                $('.username-live .username-invalid').show()
                $('.username-live .username-valid').hide()
                $('.username-live [type=submit]').addClass('button-disabled').prop('disabled', 'true')
            }
        })
    })
}

async function initPushNotifications() {
    await PushSubscriptionManager.initialize()

    let $check = $('input[name="notifications.push"]') 
    let $state = $check.parent().find('.state')

    if(PushSubscriptionManager.state.available) {
        if(PushSubscriptionManager.state.activated) {
            $check.prop('checked', true)
        } else {
            $check.prop('checked', false)
        }
        $check.prop('disabled', false)
        $state.text('')
    } else {
        $check.prop('disabled', true)
        $state.text('(not available on your device)')
    }

    $check.change(async function() {
        let $this = $(this)
        let shouldbe = null
        if($this.prop('checked')) {
            shouldbe = true
            await PushSubscriptionManager.activate()
        } else {
            shouldbe = false
            await PushSubscriptionManager.deactivate()
        }

        if(shouldbe != PushSubscriptionManager.state.activated) {
            $state.text('(Something went wrong. Try again later.)')
        } else {
            $state.text('')
        }

        if(PushSubscriptionManager.state.activated) {
            $check.prop('checked', true)
        } else {
            $check.prop('checked', false)
        }
    })
}

$(document).ready(function() {
    $(window).on('resize', $.debounce(100, justifylayout))
    setInterval(justifylayout, 2000)
    justifylayout()

    initFolderSelect()
    initPictureInput()
    initMenuScroll()
    initGrowing()
    initUsernameLive()
    initPushNotifications()

    $('#hamburger-menu-icon').click(function() {
        $('#page-header').find('.navigation').slideToggle()
        $(this).prop('name') == 'menu' ? $(this).prop('name', 'close') : $(this).prop('name', 'menu')
    })
})