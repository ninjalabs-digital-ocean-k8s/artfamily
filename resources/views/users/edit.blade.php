@extends('layouts.app')

@section('header')
    {{ __('Settings') }}
@endsection

@section('content')

<div class="three-container">

    <form method="POST" action="{{ $user->route('update') }}" class="center" enctype="multipart/form-data">
        @include('components.errors')
        @csrf
        @method('PUT')

        {{-- <div class="card float-right rounded-lg mt-8 w-96 h-48 picture-input text-grey-darkest font-light text-xl flex flex-col items-center justify-around text-center">
            <i class="mi mi-Camera mi-2x"></i>
            {{ __('Choose a profile picture') }}
            <input type="file" accepts='image/*' value="{{ old('image') }}" class="hidden" name='image'>
        </div> --}}

        <div class="card rounded-lg mt-8 mx-auto md:mx-0 w-48 h-48 picture-input text-grey-darkest font-light text-xl flex flex-col items-center justify-around text-center">
            <i class="mi mi-Camera mi-2x"></i>
            {{ __('Choose an avatar') }}
            <input type="file" accepts='image/*' value="{{ old('avatar') }}" class="hidden" name='avatar'>
        </div>

        <div class="md:mt-8 flex md:flex-row flex-col justify-between items-stretch">
            <div class="card mt-4 field mb-0">
                <label>
                    Your name
                </label>
                <input class="input" type="text" name="name" value="{{ old('name') ?? $user->name }}" placeholder="John doe">
            </div>
    
            <div class="card mt-4 field md:ml-8 mb-0">
                <label>
                    Your Email-Address
                </label>
                <input class="input" type="email" name="email" value="{{ old('email') ?? $user->email }}" placeholder="john.doe@provider.tld">
            </div>
        </div>

        <div class="card mt-8 username-live overflow-hidden">
            <div class="-mx-8 -mt-8 p-8 mb-4 bg-red-lightest text-red username-invalid hidden">
                <div class="font-bold text-sm">
                    {{ __('This username is not correct') }}
                </div>
                <ul class="mt-4 username-errors">
                    
                </ul>
            </div>

            <div class="field m-0">
                <label>
                    {{ __('Your username') }}
                </label>
                <input type="username" name="username" class="input text-3xl font-black" placeholder="{{ __('john-doe') }}" value="{{ $user->username }}" autofocus>
            </div>
        

            <div class="mt-8 username-valid">
                <div class="font-bold text-sm text-grey-darkest">
                    {{ __('Your URL will look like this') }}
                </div>
                <div class="mt-4 username-route-live">
                    {{ $user->route() }}
                </div>
            </div>
        </div>

        <div class="card mt-8">
            <div class="font-black text-grey-darkest mb-8">
                Notification Settings
            </div>
            <div class="flex flex-row items-center justify-start">
                <input type="checkbox" style="display: none" name="notifications_mail_active" id="notifications_mail_active" {{ old('notifications_mail_active') ? 'checked' : ($user->settings()->get('notifications.mail.active') ? 'checked' : '') }}>
                <label for="notifications_mail_active" class="toggle"><span></span></label>
                <div class="ml-4">
                    Receive email notifications
                </div>
            </div>
            <div class="flex flex-row items-center justify-start mt-4">
                <input type="checkbox" style="display: none" name="notifications.push" id="notifications.push" disabled>
                <label for="notifications.push" class="toggle"><span></span></label>
                <div class="ml-4">
                    Receive Push Notifications <span class="state">(loading...)</span>
                </div>
            </div>

            <div class="mt-8 font-black text-grey-darkest mt-8">
                Notify me about
            </div>
            <div class="md-checkbox mt-4">
                <input type="checkbox" name="notifications_type_NewCreation" id="notifications_type_NewCreation" {{ old('notifications_types_NewCreation') ? 'checked' : ($user->settings()->get('notifications.types.NewCreation') ? 'checked' : '') }}>
                <label for="notifications_type_NewCreation">New Creations from people I'm following</label>
            </div>
            <div class="md-checkbox mt-4">
                <input type="checkbox" name="notifications_type_NewSketch" id="notifications_type_NewSketch" {{ old('notifications_types_NewSketch') ? 'checked' : ($user->settings()->get('notifications.types.NewSketch') ? 'checked' : '') }}>
                <label for="notifications_type_NewSketch">New Sketches from people I'm following</label>
            </div>
            <div class="md-checkbox mt-4">
                <input type="checkbox" name="notifications_type_NewChallenge" id="notifications_type_NewChallenge", {{ old('notifications_types_NewChallenge') ? 'checked' : ($user->settings()->get('notifications.types.NewChallenge') ? 'checked' : '') }}>
                <label for="notifications_type_NewChallenge">New Challenges from people I'm following</label>
            </div>
            <div class="md-checkbox mt-4">
                <input type="checkbox" name="notifications_type_NewComment" id="notifications_type_NewComment" {{ old('notifications_types_NewComment') ? 'checked' : ($user->settings()->get('notifications.types.NewComment') ? 'checked' : '') }}>
                <label for="notifications_type_NewComment">New Comments on my creations, sketches and challenges</label>
            </div>
        </div>

        <button type="submit" class="button mt-16 mx-8 float-right">{{ __('Save Profile') }}</button>

    </div>

</div>

@endsection