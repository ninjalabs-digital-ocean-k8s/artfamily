@extends('layouts.app')

@section('header')
    {{ __('Users') }}
@endsection

@section('content')

<div class="card md:w-2/3 md:mx-auto mt-8">
    <div class="flex flex-col users">
        @foreach($users as $user)
            <a href="{{ $user->route() }}" class="flex flex-row p-4 items-center user">
                <img data-src="{{ $user->avatar()->url() }}" alt="Profile Picture" class="h-12 w-auto shadow-md lazyload rounded-full">
                <div class="flex flex-col ml-8">
                    <div class="font-bold">
                        {{ $user->name }}
                    </div>
                    <div class="font-light">
                        {{ '@' . $user->username }}
                    </div>
                </div>
            </a>
        @endforeach
    </div>
</div>

@endsection