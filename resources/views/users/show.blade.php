@extends('layouts.app')

@section('header')
    Profile
@endsection

@section('padding', '')

@section('content')

    <div class="overflow-hidden">
        <div class="h-64 stacking">
            <img data-src="{{ $user->cover() }}" class="blur w-full h-auto min-h-full -z-10 lazyload" style="filter: brightness(50%)blur(20px); transform: scale(2)"></img>
            <img data-src="{{ $user->cover() }}" class="h-64 w-auto z-10 lazyload" style="justify-self: center;"></img>
        </div>
        <div class="blurry bg-grey-lightest">
            <div class="three-container ">
                <div class="left flex flex-col items-center ">
                    <img data-src="{{ $user->avatar()->url() }}" alt="" class="z-10 h-48 w-auto -mt-24 drop-shadow lazyload rounded-full">
                    @auth
                        <a href="{{ $user->route('togglefollow') }}" class="button mt-8 {{Auth::user()->follows($user) ? 'green' : ''}}">
                            <i class="mi mi-{{ Auth::user()->follows($user) ? 'InkingColorFill' : 'InkingColorOutline' }} mr-4"></i>
                            {{ __(Auth::user()->follows($user) ? 'Following' : 'Follow') }}
                        </a>
                    @endauth
                </div>
                @if ($user->isLoggedIn())
                    <div class="right buttons md:mr-8">
                        <form action="{{ route('logout') }}" method="post">
                            @csrf
                            <button class="button-red border-none float-right mt-8" type="submit">
                                {{ __('Logout') }}
                            </button>
                        </form>
                        <a href="{{ $user->route('edit') }}" class="button mt-4">
                            <i class="mi mi-Settings mr-4"></i> Settings
                        </a>
                    </div>
                @endif
                <div class="center ">
                    <div class="flex flex-wrap flex-row justify-between p-8">
                        <div class="font-bold tracking-wide text-3xl">
                            {{ $user->name }}
                        </div>
                        <div class="font-light text-2xl">
                            {{ '@' . $user->username }}
                        </div>
                    </div>
                    <div class="md:justify-end justify-around p-8 navigation">
                        <div class="active item">
                            <i class="mi mi-ContactInfo"></i>
                            <div class="content">
                                {{ __('Overview') }}
                            </div>
                        </div>
                        <div class="item">
                            <i class="mi mi-Picture"></i>
                            <div class="content">
                                {{ __('Gallery') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>

    <div class="bg-white shadow-lg">
        <div class="font-black three-container mx-auto">
            <div class="center p-6">
                <div class="font-black text-grey-darkest">
                    {{ __('Latest Creations') }}
                </div>
            </div>
        </div>
        <div class="font-black mx-auto three-container">
            <div class="p-6 pt-0 whitespace-no-wrap overflow-x-auto center">
                @foreach ($user->creations as $creation)
                    <a href="{{ $creation->route('show') }}">
                        <img data-src="{{ $creation->image->url() }}" alt="" class="h-32 lazyload">
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    {{-- <div class="font-black three-container mx-auto">
        <div class="center pt-16 p-6">
            <div class="font-black text-grey-darkest">
                {{ __('Folders') }}
            </div>
        </div>
    </div>
    <div class="three-container mx-auto">
        <div class="center p-6 pt-0 flex flex-row justify-stretch whitespace-no-wrap overflow-x-auto">
            @foreach ($user->folders as $folder)
                <a href="{{ $folder->route('show') }}" style="transition: all 200ms ease-in-out" class="bg-white rounded-lg p-6 mr-4 hover:shadow-none shadow-lg flex flex-col items-start justify-between">
                    <div class="self-center clearfix p-2" style="min-width: calc(18px + 8.5em)">
                        @foreach($folder->images(5) as $image)
                            <img data-src="{{ $image->url() }}" alt="" class="float-left border-white h-12 lazyload" style="border-width: 3px;width: 4.5rem;margin-left: {{ $loop->index > 0 ? -3.5 : 0}}rem; margin-top: {{ $loop->index * 1 }}rem; ">
                        @endforeach
                    </div>
                    <div class="mt-4 text-grey-darkest">
                        <div class="font-black">
                            {{ $folder->name }}
                        </div>
                        <div class="font-light text-sm mt-2">
                            {{ $folder->images()->count() }} {{ __('Images') }}
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div> --}}

    <div class="bg-white shadow-lg mt-8">
        <div class="font-black three-container mx-auto">
            <div class="center p-6">
                <div class="font-black text-grey-darkest">
                    {{ __('Latest Sketches') }}
                </div>
            </div>
        </div>
        <div class="font-black mx-auto three-container">
            <div class="p-6 pt-0 overflow-x-auto whitespace-no-wrap center">
                @foreach ($user->sketches as $sketch)
                    <a href="{{ $sketch->route('show') }}">
                        <img data-src="{{ $sketch->image->url() }}" alt="" class="h-32 lazyload">
                    </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
