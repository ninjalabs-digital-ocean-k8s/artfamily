@extends('layouts.app')

@section('header')
    {{ __('Notifications') }}
@endsection

@section('content')

<div class="container md:mx-auto card">
    @foreach ($notifications->groupBy(function ($notification) {
        return $notification->updated_at->toDateString();
    })->sortKeysDesc() as $date => $notificationgroup)
        <div class="flex flex-row justify-between items-baseline py-8 text-grey-darkest">
            <div class="font-black">
                {{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('j. F Y') }}
            </div>
            <div class="font-light">
                {{ $notificationgroup->count() }} {{ __('Notifications') }}
            </div>
        </div>
        <div>
            @foreach ($notificationgroup as $notification)
                @includeIf('components.notifications.' . class_basename($notification->type), ['notification' => $notification])
            @endforeach
        </div>

    @endforeach

</div>

@endsection