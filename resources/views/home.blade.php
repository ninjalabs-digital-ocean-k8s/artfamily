@extends('layouts.app')

@section('header')
    {{ __('Feed') }}
@endsection

@section('content')

@guest

    <div class="flex flex-col items-center my-16 mx-8 md:mx-0">
        <img src="{{ asset('pictures/welcome.svg') }}" class="h-64">
        <span class="text-center font-light text-grey-darkest text-5xl mt-8">
            {{ __('Hello, Stranger!') }}
        </span>
        <span class="font-black text-grey-darkest text-center my-8">
            {{ __('Greetings from a site for sharing your creations and sketches.') }}
        </span>
        <div class="flex flex-row items-center justify-center">
            <a href="{{ route('login') }}" class="orange button">
                <i class="mi mi-ChevronRightSmall mr-4"></i>
                {{ __('Login') }}
            </a>
            <a href="{{ route('register') }}" class="orange button ml-8">
                <i class="mi mi-ContactSolid mr-4"></i>
                {{ __('Register') }}
            </a>
        </div>
    </div>

@else

    <div class="three-container">

        <div class="right buttons">

            <a href="{{ route('creations.create') }}" class="button mt-8">
                New Creation
            </a>
            <a href="{{ route('sketches.create') }}" class="button mt-8">
                New Sketch
            </a>

        </div>

        <div class="center mx-8 md:mx-0">

            @include('components.imagables', [ 'imagables' => $imagables])

            @includeWhen($followsnoone, 'components.emptyfeed', ['some' => 'data'])

        </div>

    </div>

@endguest

@endsection
