@extends('layouts.app')

@section('header')
    {{ __( ($imagable ? 'Update ' : 'New ') . $model->name) }}
@endsection

@section('content')

    @include('components.errors')

    <form method="POST" action="{{ $imagable ? route($model->route . '.update', compact('imagable')) : route($model->route . '.store') }}" class="three-container three-container-between" enctype="multipart/form-data">
        @csrf
        @if ($imagable)
            @method('PUT')
        @endif

        @if(isset($belonging))
            <input type="hidden" name="{{ $belonging->key }}" value="{{ $belonging->value }}">
        @endif

        @if($model->static::has('image') || $model->static::has('images'))
            <div class="left">
                <div class="card rounded-lg mx-auto mb-8 md:mb-0 w-48 h-48 picture-input text-grey-darkest font-light text-xl flex flex-col items-center justify-around text-center">
                    <i class="mi mi-Camera mi-2x"></i>
                    @if($model->static::has('image'))
                        {{ __('Choose a Picture') }}
                    @else
                        {{ __('Choose Pictures') }}
                    @endif
                    <input type="file" accepts='image/*' value="{{ old('image') }}" class="hidden" 
                        @if ($model->static::has('images'))
                            multiple
                            name="image[]"
                        @else
                            name='image'
                        @endif
                    >
                </div>
            </div>
        @endif

        <div class="center">
            @if ($model->static::has('caption') && !$model->static::has('ComputedCaption'))
                <div class="{{ $errors->has('caption') ? 'error' : '' }} card md:max-w-md md:mx-auto field">
                    <label class="font-light"> {{ __('Caption') }} </label>
                    <input class="input text-2xl font-black text-grey-darkest" type="text" name="caption" placeholder="{{ __('My awesome') }} {{ __($model->name) }}" value="{{ old('caption') ?: $imagable ? $imagable->caption : null }}" autofocus>
                </div>
            @endif

            <div class="{{ $errors->has('description') ? 'error' : '' }} card md:max-w-md md:mx-auto field">
                <label> {{ __('Description') }} </label>
                <textarea class="input" name="description" rows="5" placeholder="{{ __('It took me 5 hours to paint something amazing like this!') }}">{{ old('description') ?? $imagable ? $imagable->description : null }}</textarea>
            </div>

            {{-- @if ($model->static::has('folders'))
                <div class="card max-w-md mx-auto field">
                    <label>
                        {{ __('Folders') }}
                    </label>
                    <div class="flex flex-row items-center justify-start -ml-2 mt-4 folder-select">
                        @foreach (Auth::user()->folders as $folder)
                            <div class="folder-tag" data-id="{{ $folder->id }}">
                                <i class="mi mi-Folder mr-2"></i>
                                {{ $folder->name }}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif --}}
        </div>

        <div class="right">
            <button type="submit" class="my-4 md:my-0 float-right button">
                {{ __( ($imagable ? 'Update ' : 'Publish ') . $model->name) }}
            </button>
        </div>

    </form>

@endsection