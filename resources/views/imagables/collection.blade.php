{{-- <div class="ui equal width grid">

    @foreach(rowify($images->take(9), 3) as $imagerow)
        <div class="row">

            @foreach ($imagerow as $image)
                <div class="column">
                    <img data-src="{{ $image->url() }}" class="ui tiny rounded image lazyload cui-square-image">
                </div>
            @endforeach

        </div>
    @endforeach

</div> --}}

<div class="cui-collection-grid">

    @foreach($images as $image)
        <img data-src="{{ $image->url() }}" class="lazyload cui-square-image">
    @endforeach

</div>