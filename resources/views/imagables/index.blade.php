@extends('layouts.app')

@section('header')
    {{ __($model->Names) }}
@endsection

@section('background', 'white')

@section('content')

    <div class="three-container">
        <div class="center mx-8 md:mx-0">
            @include('components.imagables', ['imagables' => $imagables])
        </div>
        <div class="right clearfix">
            <a href="{{ route($model->route . '.create') }}" class="button float-right mt-8 mr-8 md:mr-4 md:mt-32">
                New {{ $model->Name }}
            </a>
        </div>
    </div>
    
    {{-- <div class="ui clearing segment">
        <h4 class="ui horizontal divider header"> {{ __('Actions') }} </h4>

        <a href="{{ route($model->route . '.create') }}" class="ui primary right floated button">
            <i class="plus icon"></i>
            {{ __('New ' . $model->name) }}
        </a>        
    </div> --}}

    {{-- @include('components.imagables', compact('imagables')) --}}

@endsection