@extends('layouts.app')

@section('header')
    {{ $model->Name }}
@endsection

@section('padding', 'px-0')

@section('content')

<div class="three-container three-container-between md:mt-8">

    @can('update', $imagable)
        <div class="right buttons mr-8">
            <a href="{{ $imagable->route('edit') }}" class="button-blue border-none mt-8 md:mt-0">
                <i class="mi mi-Edit mr-4"></i>
                {{ __('Edit') }}
            </a>
            <form action="{{ $imagable->route('destroy') }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="button-red border-none mt-8">
                    <i class="mi mi-Delete mr-4"></i>
                    {{ __('Delete') }}
                </button>
            </form>
        </div>
    @endcan

    <div class="center flex flex-col items-center md:px-8">

        <img data-src="{{ $imagable->image->big() }}" alt="" class="lazyload mx-auto md:rounded-lg z-10" style="max-height: 80vh">

        <div class="card self-stretch md:mt-8">
            <div class="flex flex-row justify-start items-center text-grey-darkest mb-8">
                <img data-src="{{ $imagable->user->avatar()->url() }}" alt="" class="lazyload h-12 rounded-full">
                <div class="flex flex-col justify-start items-start ml-4">
                    @if ($imagable->has('CaptionLink'))
                        <a href="{{ $imagable->caption_link }}" class="link font-black text-3xl">
                            {{ $imagable->caption }} 
                        </a>
                    @else
                        <div class="font-black text-3xl">
                            {{ $imagable->caption }} 
                        </div>
                    @endif
                    <div class="font-light text-center ">
                    {{ __('by') }} <a class="link" href="{{ $imagable->user->route() }}">{{ $imagable->user->name }}</a>
                    </div>
                </div>
            </div>
            @if ($imagable->description)
                {!! nl2br(e($imagable->description)) !!}
            @endif
            <div class="-mb-8 -mx-8 border-t-2 border-grey-lightest mt-8">
                <a href="{{ $imagable->image->full() }}" class="transition border-r-2 border-grey-lightest px-8 py-4 text-sm text-grey-darker font-bold inline-flex flex-row items-center hover:bg-black hover:text-white" download>
                    <i class="mi mi-Download mr-2"></i> Download full image
                </a>
            </div>
        </div>
    </div>

</div>

@if($imagable->has('comments'))
<div class="bg-white shadow-lg mt-16">
    <div class="three-container">

        <div class="center p-8">
            
            <div class="font-black text-grey-darkest">
                {{ __('Comments') }}
            </div>
            <div class="mt-4">
                <div class="mb-8">
                    @include('comments.create', [ 'commentable' => $imagable ])
                </div>
                @include('comments.list', [ 'comments' => $imagable->commentsSorted() ])
            </div>
    
        </div>
    
    </div>
</div> 
@endif

{{-- <div class="ui stackable grid">
    <div class="row">

        <div class="left floated ten wide column">
            <img class="ui centered rounded image cui-fullsize-imagable" src="{{ $imagable->image->url() }}" alt="">
        </div>

        <div class="right floated six wide stretched column">
            <div class="ui segment">
                <h4 class="ui horizontal divider header">{{ __('More from') }} {{ $imagable->user->name }}</h4>
                @include('imagables.collection', [ 'images' => $imagable->user->images() ])
            </div>
        </div>

    </div>
    <div class="row">
        <div class="sixteen wide column">
            <div class="ui segment">
                @include('components.user', [ 'user' => $imagable->user ])

                @if ($imagable->description)
                    <div class="ui list">
                        <div class="item">
                            <i class="grey bold icon"></i>
                            <div class="content">
                                <div class="description">
                                    {{ $imagable->description }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div> --}}
        {{-- <div class="ui card">
            <div class="center aligned content">
                <div class="header">
                    {{ $imagable->caption }}
                </div>
            </div>
            <div class="center aligned content">
                <div class="meta">
                    <i class="clock icon"></i> {{ $imagable->created_at->diffForHumans() }}
                </div>
            </div>
            @if ($imagable->description)
                <div class="center aligned content">
                    <div class="description">
                        {!! nl2br(e($imagable->description)) !!}
                    </div>
                </div>
            @endif
            <a href="{{ route('users.show', $imagable->user) }}" class="extra content">
                <div class="center aligned author">
                    <img class="ui avatar image" src="{{ asset('/pictures/logo.png') }}"> {{ $imagable->user->name }}
                </div>
            </a>
            @can('update', $imagable)
                <div class="ui bottom attached buttons">
                    <a href="{{ route($model->route . '.edit', $imagable) }}" class="ui primary button">
                        <i class="pencil icon"></i>
                        {{ __('Edit') }}
                    </a>
                    <form action="{{ route($model->route . '.destroy', $imagable) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="ui primary basic button">
                            <i class="trash icon"></i>
                            {{ __('Delete') }}
                        </button>
                    </form>
                </div>
            @endcan
        </div> --}}
    </div>
</div>

@endsection