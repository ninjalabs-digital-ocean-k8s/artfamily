@extends('layouts.app')

@section('header')
    {{ __('403 Page') }}
@endsection

@section('content')

    <div class="three-container">

        <div class="center">

            <div class="flex flex-col items-center my-16">
                <img class="mx-auto h-64" src="{{ asset('pictures/access-denied.svg') }}">
                <span class="text-grey-darkest font-light my-8 text-5xl text-center">
                    {{ __('You are not allowed to see this page.') }}
                </span>
                @guest
                    <span class="font-black text-grey-darkest text-center">
                        {{ __('This could be because you are not logged in. Try to log in or register!') }}
                    </span>
                    <div class="flex flex-row items-center justify-center mt-8">
                        <a href="{{ route('login') }}" class="button-blue">
                            <i class="mi mi-ChevronRightSmall mr-4"></i>
                            {{ __('Login') }}
                        </a>
                        <a href="{{ route('register') }}" class="button-blue ml-8">
                            <i class="mi mi-ContactSolid mr-4"></i>
                            {{ __('Register') }}
                        </a>
                    </div>
                @else
                    <span class="font-black text-grey-darkest text-center">
                        {{ __('Seems like you do not have the neccesary access rights to view this page. Maybe you are trying to edit or delete a thing, thats not yours.') }}
                    </span>
                    <div class="flex flex-row items-center justify-center mt-8">
                        <a href="{{ url()->previous() }}" class="button-blue">
                            <i class="mi mi-ChevronLeftSmall mr-4"></i>
                            {{ __('Go back') }}
                        </a>
                    </div>
                @endguest
            </div>

        </div>

    </div>


@endsection