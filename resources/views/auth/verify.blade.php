@extends('layouts.app')

@section('header')
    Verify Email
@endsection

@section('content')
<div class="three-container">
    <div class="center">
        @include('components.auth.verify')
    </div>
</div>
@endsection
