@extends('layouts.app')

@section('header')
    {{ __('Login') }}
@endsection

@section('background', 'grey-white-gradient')

@section('content')

@include('components.errors')
<form class="max-w-md mx-auto bg-white shadow-lg p-6 m-4 rounded-lg form" method="POST" action="{{ route('login') }}">
    @csrf

    <div class="field {{ $errors->has('email') ? 'error' : '' }}">
        <label>{{ __('E-Mail Address') }}</label>
        <input class="input" type="email" name="email" value="{{ old('email') }}" placeholder="john.doe@company.com" required autofocus>
    </div>

    <div class="field {{ $errors->has('password') ? 'error' : '' }}">
        <label>{{ __('Password') }}</label>

        <input class="input" type="password" name="password" required placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
    </div>

    <div class="field">
        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
        <label>
            {{ __('Remember Me') }}
        </label>
    </div>

    <div class="flex flex-row justify-between items-center pt-4">
        <button type="submit" class="button">
            {{ __('Login') }}
        </button>
    
        <a class="link" href="{{ route('password.request') }}">
            {{ __('Forgot Your Password?') }}
        </a>
    </div>
</form>
@endsection
