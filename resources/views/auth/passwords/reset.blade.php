@extends('layouts.app')

@section('header')
    {{ __('Reset Password') }}
@endsection

@section('background', 'grey-white-gradient')

@section('content')

    @include('components.errors')

    <form method="POST" action="{{ route('password.request') }}" class="container mx-auto card form">
        @csrf

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="field {{ $errors->has('email') ? 'error' : '' }}">
            <label>{{ __('E-Mail Address') }}</label>
            <input class="input" type="email" name="email" value="{{ $email ?? old('email') }}" placeholder="john.doe@company.com" required autofocus>
        </div>

        <div class="field {{ $errors->has('password') ? 'error' : '' }}">
            <label>{{ __('Password') }}</label>
            <input class="input" type="password" name="password" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;" required>
        </div>

        <div class="field {{ $errors->has('password_confirmation') ? 'error' : '' }}">
            <label>{{ __('Confirm Password') }}</label>
            <input class="input" type="password" name="password_confirmation" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;" required>
        </div>
        <button type="submit" class="button">
            {{ __('Reset Password') }}
        </button>
    </form>
@endsection
