@extends('layouts.app')

@section('header')
    {{ __('Reset Password') }}
@endsection

@section('background', 'grey-white-gradient')

@section('content')

    @include('components.errors')

    <form method="POST" action="{{ route('password.email') }}" class="container mx-auto card form">
        @csrf

        <div class="field {{ $errors->has('email') ? 'error' : '' }}">
            <label>{{ __('E-Mail Address') }}</label>
            <input class="input" type="email" name="email" value="{{ old('email') }}" placeholder="john.doe@company.com" required autofocus>
        </div>

        <button type="submit" class="button mt-4">
            {{ __('Send Password Reset Link') }}
        </button>
    </form>
@endsection
