@extends('layouts.app')

@section('header')
    {{ __('Register') }}
@endsection

@section('background', 'grey-white-gradient')

@section('content')

@include('components.errors')

    <form method="POST" action="{{ route('register') }}" class="max-w-md mx-auto bg-white shadow-lg rounded-lg p-8 form clearfix">
        @csrf

        <div class="field {{ $errors->has('name') ? ' error' : '' }}">
            <label>{{ __('Name') }}</label>
            <input class="input" type="text" name="name" value="{{ old('name') }}" placeholder="John Doe" required autofocus>
        </div>

        <div class="field {{ $errors->has('email') ? ' error' : '' }}">
            <label>{{ __('E-Mail Adress') }}</label>
            <input class="input" type="email" name="email" value="{{ old('email') }}" placeholder="john.doe@domain.com" required>
        </div>

        <div class="field {{ $errors->has('username') ? ' error' : '' }}">
            <label>{{ __('Username') }}</label>
            <input class="input" type="text" name="username" value="{{ old('username') }}" placeholder="johndoe24" required>
        </div>

        <div class="field {{ $errors->has('password') ? ' error' : '' }}">
            <label>{{ __('Password') }}</label>
            <input class="input" type="password" name="password" value="{{ old('password') }}" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;" required>
        </div>

        <div class="field {{ $errors->has('password-confirm') ? ' error' : '' }}">
            <label>{{ __('Confirm Password') }}</label>
            <input class="input" type="password" name="password_confirmation" value="{{ old('password-confirmation') }}" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;" required>
        </div>

        <button type="submit" class="float-right button">
            {{ __('Register') }}
        </button>
    </form>
@endsection
