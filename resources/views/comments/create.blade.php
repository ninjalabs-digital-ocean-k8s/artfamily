<form class="flex flex-col md:flex-row items-center justify-stretch" method="POST" action="{{ route('comments.store') }}">
    @csrf
    <input type="hidden" name="commentable_type" value="{{ get_class($commentable) }}">
    <input type="hidden" name="commentable_id" value="{{ $commentable->id }}">
    <textarea name="body" placeholder="{{ __('Awesome!') }}" rows="1" class="input bg-grey-lightest rounded p-4 md:mr-4 mt-0 growing" value="{{ old('body') }}"></textarea>
    <button type="submit" class="button self-end md:self-auto mt-4 md:mt-0">
        {{ __('Comment') }}
    </button>
</form>