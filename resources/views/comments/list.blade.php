@foreach ($comments as $comment)
    
    <div id="comment:{{ $comment->id }}" class="flex flex-row items-start justify-start -mx-8 px-8 py-4">
        <img src="{{ $comment->user->avatar()->url() }}" alt="" class="rounded-full h-12">
        <div class="ml-4">
            <div class="flex flex-row flex-wrap md:flex-no-wrap items-center justify-start mb-2">
                <a href="{{ $comment->user->route() }}" class="font-bold link text-black">
                    {{ $comment->user->name }}
                </a>
                <span class="text-grey-darkest font-light ml-4">
                    {{ $comment->updated_at->diffForHumans() }}
                </span>
                @can('delete', $comment)
                    <form action="{{ $comment->route('destroy') }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="text-red ml-8 flex flex-row items-center justify-center text-sm">
                            <i class="mi mi-Delete mr-2"></i>
                            {{ __('Delete') }}
                        </button>
                    </form>
                @endcan
            </div>
            <div class="">
                {!! nl2br(e($comment->body)) !!}
            </div>
            {{-- <div class="text-grey-darkest font-light text-sm mt-2 flex flex-row justify-start items-center">
                @if($comment->comments->count() > 0)
                    <div class="font-bold cursor-pointer mr-2">
                        {{ __('Show Responses') }} ({{ $comment->comments->count() }})
                    </div>
                @endif
                @can('create', Artfamily\Comment::class)
                    @if($comment->comments->count() > 0)
                        <div class="mr-2">
                            &bull;
                        </div>
                    @endif
                    <div class="cursor-pointer">
                        {{ __('Reply') }}
                    </div>
                @endcan
            </div> --}}
        </div>
    </div>
    {{-- <div class="ml-6 mt-4 border-grey border-l-2 p-4 {{ $comment->comments->count() > 0 ? '' : 'hidden' }}">
        <div class="mb-8 text-sm">
            @include('comments.create', [ 'commentable' => $comment ])
        </div>
        @include('comments.list', [ 'comments' => $comment->commentsSorted() ])
    </div> --}}

@endforeach