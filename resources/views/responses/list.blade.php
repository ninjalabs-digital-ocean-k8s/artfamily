@foreach ($responses as $response)
    <div class="flex flex-col items-start justify-start px-8 border-grey-lightest border-b-2 pb-8 {{ $loop->index > 0 ? 'mt-8' : 'mt-4'}}">
        <div class="w-full flex flex-row flex-wrap md:flex-no-wrap items-center justify-between mb-2">
            <div class="flex flex-row items-center">
                <img data-src="{{ $response->user->avatar()->url() }}" alt="" class="rounded-full h-8 lazyload">
                <a href="{{ $response->user->route() }}" class="font-bold link text-black ml-4">
                    {{ $response->user->name }}
                </a>
            </div>
            <div class="flex flex-row items-center">
                <span class="text-grey-darkest font-light ml-4">
                    {{ $response->updated_at->diffForHumans() }}
                </span>
                @can('delete', $response)
                    <form action="{{ $response->route('destroy') }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="text-red ml-8 flex flex-row items-center justify-center text-sm">
                            <i class="mi mi-Delete mr-2"></i>
                            {{ __('Delete') }}
                        </button>
                    </form>
                @endcan
            </div>
        </div>
        <div class="flex flex-col-reverse md:flex-row justify-between w-full items-start">
            <div class="mt-2 md:mt-0 md:mr-4 md:ml-12">
                {!! nl2br(e($response->description)) !!}
            </div>
            <a href="{{ $response->route() }}">
                <img data-src="{{ $response->image->url() }}" alt="" class="w-32 rounded shadow lazyload">
            </a>
        </div>
    </div>
@endforeach