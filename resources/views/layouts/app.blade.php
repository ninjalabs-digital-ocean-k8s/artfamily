<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="/manifest.json">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Icon --}}
    <link rel="icon" href="{{ asset('pictures/logo.jpg') }}">

    <title>
        @yield('header') - {{ config('app.name', 'Laravel') }}
    </title>

    <!-- Scripts -->
    <script>
        window.vapidPublicKey = "{{ config('webpush.vapid.public_key') }}"
    </script>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/throttle.js') }}"></script>
    <script src="{{ asset('js/autosize.min.js') }}"></script>
    <script src="{{ asset('js/lazyload.js') }}" defer async></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/micon.min.css') }}">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    {{-- <link href="{{ asset('semantic/dist/components/transition.min.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/semantic.min.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>

<body class="min-h-screen bg-@yield('background', 'grey-lightest')">
    <div class="bg-grey-lightest p-6 md:p-8 flex flex-col md:flex-row justify-between sticky pin-t z-50 blurry transition" id="page-header">
        <div class="flex flex-row justify-between items-center">
            <div class="flex flex-col text-grey-darkest">
                <a href="/" class="text-xs md:text-base font-black" id="brand">
                    {{ config('app.name', 'Greymatter') }}
                </a>
                <div class="text-3xl md:text-5xl font-black">
                    @yield('header')
                </div>
            </div>
            <i class="text-3xl mi mi-GlobalNavButton md:hidden" id="hamburger-menu-icon"></i>
        </div>
        <div class="navigation">
            <a href="{{ route('home') }}" class="{{ starts_with(Route::currentRouteName(), 'home') ? 'active' : '' }} item">
                <i class="mi mi-Home"></i>
                <div class="content">
                    {{ __('Feed') }}
                </div>
            </a>

            <a href="{{ route('creations.index') }}" class="{{ starts_with(Route::currentRouteName(), 'creations') ? 'active' : '' }} item">
                <i class="mi mi-Photo2"></i>
                <div class="content">
                    {{ __('Creations') }}
                </div>
            </a>

            <a href="{{ route('sketches.index') }}" class="{{ starts_with(Route::currentRouteName(), 'sketches') ? 'active' : '' }} item">
                <i class="mi mi-Handwriting"></i>
                <div class="content">
                    {{ __('Sketches') }}
                </div>
            </a>

            <a href="{{ route('challenges.index') }}" class="{{ starts_with(Route::currentRouteName(), 'challenges') ? 'active' : '' }} item">
                <i class="mi mi-Communications"></i>
                <div class="content">
                    {{ __('Challenges') }}
                </div>
            </a>

            <a href="{{ route('users.index') }}" class="{{ starts_with(Route::currentRouteName(), 'users') ? 'active' : '' }} item">
                <i class="mi mi-Contact"></i>
                {{ __('Users') }}
            </a>
        </div>
        <div class="navigation">
            @guest
                <a class="{{ Route::currentRouteNamed('login') ? 'active' : '' }} item" href="{{ route('login') }}">
                    {{ __('Login') }}
                </a>
                <a class="{{ Route::currentRouteNamed('register') ? 'active' : '' }} item" href="{{ route('register') }}">
                    {{ __('Register') }}
                </a>
            @else
                <a href="{{ Auth::user()->route('notifications') }}" class="{{ ends_with(Route::currentRouteName(), 'notifications') ? 'active' : '' }} item">
                    <div class="{{ Auth::user()->unreadNotifications()->count() > 0 ? 'bg-orange rounded-full p-2 text-white font-bold' : '' }} flex flex-row items-center">
                        @if (Auth::user()->unreadNotifications()->count() > 0)
                            <i class="mi mi-Ringer mr-4"></i>
                            {{ Auth::user()->unreadNotifications()->count() }}
                        @else
                            <i class="mi mi-Ringer"></i>
                        @endif
                    </div>  
                </a>
                <a href="{{ Auth::user()->route('show') }}" class="item flex flex-row items-center loggedinuser">
                    <img src="{{ Auth::user()->avatar()->url() }}" alt="" class="w-auto h-8 shadow-md rounded-full">
                    <div class="font-bold text-grey-darker ml-4">
                        {{ Auth::user()->name }}
                    </div>
                </a>
            @endguest
        </div>
    </div>
    <div class="@yield('padding', 'md:px-8')">
        @if (Auth::user() && !Auth::user()->hasVerifiedEmail())
            <div class="mx-auto container my-8">
                @include('components.auth.verify')
            </div>
        @endif
        @yield('content')
    </div>
</body>
</html>
