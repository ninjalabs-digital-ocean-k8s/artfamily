<a href="{{ isset($link) ? $link : $subject->route() }}" class="flex flex-row items-start justify-start hover:bg-grey-lightest -mx-8 px-8 py-4 {{ $notification->unread() ? 'bg-blue-lightest' : '' }}">

    <img src="{{ isset($icon) ? $icon : $subject->user->avatar()->url() }}" class="rounded-full h-10 w-10 mr-4" alt="">

    <div class="flex flex-col items-start">
        <div class="font-bold">{{ $title }}</div>
        <div class="text-grey-darker mt-2">{{ $slot }}</div>
    </div>

</a>