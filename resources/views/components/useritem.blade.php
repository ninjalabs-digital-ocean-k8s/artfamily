<div class="item">
    <img src="{{ asset('pictures/logo.png') }}" class="ui middle aligned avatar image">
    <div class="content">
        <div class="header">
            {{ $user->name }}
        </div>
        <div class="description">
            {{ $user->email }}
        </div>
    </div>
</div>