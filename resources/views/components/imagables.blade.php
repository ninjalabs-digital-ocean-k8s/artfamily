@foreach ($imagables->groupBy(function ($imagable) {
    return $imagable->updated_at->toDateString();
})->sortKeysDesc() as $date => $imagablegroup)
    <div class="flex flex-row justify-between items-baseline py-8 text-grey-darkest">
        <div class="font-black">
            {{ Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('j. F Y') }}
        </div>
        <div class="font-light">
            {{ $imagablegroup->count() }} {{ __('Images') }}
        </div>
    </div>
    <div class="justified-layout">
        @foreach ($imagablegroup as $imagable)
            <a href="{{ $imagable->route() }}" class="stacking imagable">
                <img data-aspectratio="{{ $imagable->image->aspect_ratio }}" data-src="{{ $imagable->image->url() }}" alt="" class="lazyload">
                <div class="hidden">
                    <div class="caption">
                        {{ $imagable->caption }}
                    </div>
                    <div class="user">
                        {{ $imagable->user->name }}
                    </div>
                </div>
            </a>
        @endforeach
    </div>
@endforeach