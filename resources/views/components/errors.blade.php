
@if (count($errors))
    <div class="container mx-auto rounded-lg bg-red-lighter mb-8 p-8">
        <div class="text-red-darker font-bold">{{ __('There were some errors') }}</div>
        <ul>
            @foreach ($errors->all() as $error)
                <li>
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    </div>
@endif