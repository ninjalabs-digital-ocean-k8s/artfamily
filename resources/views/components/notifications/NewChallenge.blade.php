@php
    $challenge = \Artfamily\Challenge::find($notification->data['id'])
@endphp

@if ($challenge)
    @component('layouts.notification', [ 
            'subject' => $challenge,
            'notification' => $notification,
        ])

        @slot('title')
            A new Challenge by {{ $challenge->user->name }} 
        @endslot

        {{ $challenge->user->name }} has just uploaded a new challenge!
    @endcomponent
@endif