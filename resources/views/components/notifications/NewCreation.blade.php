@php
    $creation = \Artfamily\Creation::find($notification->data['id'])
@endphp

@if($creation)
    @component('layouts.notification', [ 
            'subject' => $creation,
            'notification' => $notification,
        ])

        @slot('title')
            A new Creation by {{ $creation->user->name }} 
        @endslot

        {{ $creation->user->name }} has just uploaded a new creation!
    @endcomponent
@endif