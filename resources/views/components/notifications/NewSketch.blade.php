@php
    $sketch = \Artfamily\Sketch::find($notification->data['id'])
@endphp

@if ($sketch)
    @component('layouts.notification', [ 
            'subject' => $sketch,
            'notification' => $notification,
        ])

        @slot('title')
            A new Sketch by {{ $sketch->user->name }} 
        @endslot

        {{ $sketch->user->name }} has just uploaded a new sketch!
    @endcomponent
@endif