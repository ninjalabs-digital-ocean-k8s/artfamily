@php
    $comment = \Artfamily\Comment::find($notification->data['id'])
@endphp

@if ($comment)
    @component('layouts.notification', [ 
            'subject'=> $comment,
            'notification' => $notification,
        ])

        @slot('title')
            {{ $comment->user->name }} has commmented
        @endslot

        {{ $comment->user->name }}: {{ $comment->body }}
    @endcomponent
@endif