<a class="masonry-item" href="{{ $imagable->route('show') }}">
    <img data-src="{{ $imagable->image->url() }}" alt="" class="ui rounded image lazyload">
</a>