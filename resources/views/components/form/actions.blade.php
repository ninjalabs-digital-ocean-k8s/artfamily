<div class="ui clearing segment">
    <button type="submit" class="ui primary right floated button">
        {{ $slot }}
    </button>
    <a href="{{ url()->previous() }}" class="ui basic primary button">
        Cancel
    </a>
</div>