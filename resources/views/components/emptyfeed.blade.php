<div class="flex flex-col items-center my-16">
    <img class="mx-auto h-64" src="{{ asset('pictures/empty.svg') }}">
    <span class="text-grey-darkest font-light my-8 text-5xl text-center">
        {{ __('Looks empty here...') }}
    </span>
    <span class="font-black text-grey-darkest text-center">
        {{ __('Go ahead and look for users to follow!') }}
    </span>
    <a href="{{ route('users.index') }}" class="blue button mt-8 flex flex-row items-center justify-center">
        <i class="mi mi-ChevronRightSmall mr-4"></i>
        {{ __('Users') }}
    </a>
</div>