<div class="card">
    <div class="font-bold text-orange mb-4 flex flex-row items-center"> <i class="mi mi-Warning mr-4"></i> {{ __('Verify Your Email Address') }}</div>

    <div class="font-light">
        @if (session('resent'))
            <span class="text-green px-4 py-2 bg-green-lightest rounded">
                {{ __('A fresh verification link has been sent to your email address.') }}
            </span>
        @endif
        
        <div class="mt-4">
            {{ __('Before proceeding, please check your email for a verification link.') }}
            {{ __('If you did not receive the email') }}, <a class="link" href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
        </div>
    </div>
</div>