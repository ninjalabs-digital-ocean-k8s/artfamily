@extends('layouts.app')

@section('header')
    {{ __($model->Names) }}
@endsection

@section('background', 'grey-lightest')

@section('content')
    
    <div class="three-container">

        <div class="right buttons">
            <a href="{{ route('challenges.create') }}" class="button mt-8">
                {{ __('New ' . $model->name) }}
            </a>
        </div>

        <div class="center">
            <div class="card mt-8 p-0">
                @include('challenges.list', ['challenges' => $imagables])
            </div>
        </div>

    </div>

@endsection