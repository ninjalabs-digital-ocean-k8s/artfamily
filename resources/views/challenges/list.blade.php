@foreach ($challenges as $challenge)
    <div class="p-8 {{ !$loop->last ? 'border-b-2 border-grey-lightest' : '' }}">
        <a href="{{ $challenge->route() }}" class="link font-bold text-black mb-2">
            {{ $challenge->caption }}
        </a>
        <div class="text-grey-darker text-sm mb-4 overflow-hidden"  style="max-height: 2rem;">
            {{ $challenge->description }}
        </div>
        <div class="flex flex-row items-center justify-start">
            <img data-src="{{ $challenge->user->avatar()->url() }}" alt="" class="h-6 mr-2 lazyload rounded-full">
            <a href="{{ $challenge->user->route() }}" class="link text-sm">
                {{ $challenge->user->name }}
            </a>
        </div>
    </div>
@endforeach