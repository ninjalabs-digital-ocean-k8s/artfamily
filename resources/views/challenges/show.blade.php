@extends('layouts.app')

@section('header')
    {{ $model->Name }}
@endsection
@section('content')

    <div class="three-container-between three-container mt-8">

        <div class="center clearfix">

            <div class="card">
                <div class="font-bold text-black text-lg mb-4">
                    {{ $imagable->caption }}
                </div>
                <div class="text-grey-darker">
                    {{ $imagable->description }}
                </div>
                <div class="flex flex-row items-center justify-start mt-8">
                    <img data-src="{{ $imagable->user->avatar()->url() }}" alt="" class="rounded-full h-8 w-8 mr-4 lazyload">
                    <a href="{{ $imagable->user->route() }}" class="link">
                        {{ $imagable->user->name }}
                    </a>
                </div>
            </div>

            <div class="card mt-8 p-0 overflow-hidden">
                <div class="font-black p-8">
                    <div class="center">
                        <div class="font-bold text-grey-darkest">
                            {{ __('Latest Responses') }}
                        </div>
                    </div>
                </div>
                <div class="p-8 pt-0 whitespace-no-wrap overflow-x-auto center">
                    @foreach ($imagable->responses()->latest()->get() as $response)
                        <a href="{{ $response->route('show') }}">
                            <img data-src="{{ $response->image->url() }}" alt="" class="h-32 lazyload">
                        </a>
                    @endforeach
                </div>
            </div>

            @can('create', \Artfamily\Response::class)
                <a href="{{ route('responses.create', $imagable) }}" class="button m-8 md:mx-0 float-right">
                    {{ __('Respond to this Challenge') }}
                </a>
            @endcan

        </div>
        
        <div class="right buttons">
        
            @can('update', $imagable)
                <a href="{{ $imagable->route('edit') }}" class="button-blue border-none mt-8 md:mt-0">
                    <i class="mi mi-Edit mr-4"></i>
                    {{ __('Edit') }}
                </a>
            @endcan
            @can('delete', $imagable)
                <form action="{{ $imagable->route('destroy') }}">
                    <button type="submit" class="button-red border-none mt-8">
                        <i class="mi mi-Delete mr-4"></i>
                        {{ __('Delete') }}
                    </button>
                </form>
            @endcan

        </div>

    </div>

    <div class="three-container mt-4">
        <div class="center">
            <div class="card p-0">
                <div class="font-bold text-grey-darkest p-8">
                    {{ __('Other Responses') }}
                </div>
                <div class="pb-8">
                    @include('responses.list', [ 'responses' => $imagable->responsesSorted() ])
                </div>
            </div>
        </div>
    </div>
@endsection