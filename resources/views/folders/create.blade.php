@extends('layouts.app')

@section('header')
    {{ __('New folder') }}
@endsection

@section('content')

    @include('components.errors')

    <form class="ui form" method="POST" action="{{ route('folders.store') }}">
        @csrf

        <div class="ui segment">
            <h4 class="ui horizontal divider header">
                {{ __('Create your folder') }}
            </h4>
    
            <div class="{{ $errors->has('name') ? 'error' : '' }} field">
                <label> {{ __('Name of your folder') }} </label>
                <input type="text" name="name" value=" {{ old('name') }} ">
            </div>

            <div class="{{ $errors->has('icon') ? 'error' : '' }} field">
                <label> {{ __('Choose an icon for your folder') }} </label>
                <div class="ui search selection dropdown">
                    <input name="icon" type="hidden" value="{{ old('icon') }}">
                    <i class="dropdown icon"></i>
                    <div class="default text">{{ __('Choose Icon') }}</div>
                    
                    <div class="menu">
                        @foreach (\Artfamily\Folder::$icons as $icon)
                            <div class="item" data-value="{{ $icon }}">
                                <i class="{{ $icon }} icon"></i>{{ $icon }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        @component('components.form.actions')
            {{ __('Create folder') }}
        @endcomponent

    </form>

@endsection