@extends('layouts.app')

@section('header')
    {{ __('Folder') }}
@endsection

@section('background', 'white')

@section('padding', 'px-2')

@section('content')

<div class="three-container">

    <div class="left clearfix">

            <div class="md:card md:float-left md:bg-grey-darkest md:text-white mx-4 my-8 text-center font-black text-grey-darkest text-xl flex flex-row md:flex-col items-center justify-center"> 
                <i class="mi mi-Folder mr-4 md:mr-0 md:mb-8"></i>
                {{ $folder->name }}
            </div>

    </div>

    <div class="right buttons">

        @can('insert', $folder)
            <a href="{{ route('creations.create', [ 'folder' => $folder ]) }}" class="mt-8 button">
                {{ __('Upload creation') }}
            </a>
            <a href="{{ route('sketches.create', [ 'folder' => $folder ]) }}" class="mt-8 button">
                {{ __('Upload sketch') }}
            </a>
        @endcan

    </div>

    <div class="center">

        @include('components.imagables', ['imagables' => $folder->imagables()])

    </div>

</div>

    {{-- <div class="ui clearing segment">
        <h4 class="ui horizontal divider header">
            {{ __('Folder ') . $folder->name }}
        </h4>

        @can('insert', $folder)
            <a href="{{ route('creations.create', [ 'folder' => $folder ]) }}" class="ui primary right floated button">
                <i class="plus icon"></i>
                {{ __('Upload creation to this folder') }}
            </a>
        @endcan
    </div>

    @if (count($folder->creations))
        <h4 class="ui horizontal divider header">
            {{ __('Creations') }}
        </h4>
        @include('components.imagables', [ 'imagables' => $folder->creations ])
    @endif

    @if (count($folder->sketches))
        <h4 class="ui horizontal divider header">
            {{ __('Sketches') }}
        </h4>
        @include('components.imagables', [ 'imagables' => $folder->sketches ])
    @endif --}}

@endsection