<?php

namespace Sebas\NamedModelRoutesProvider;

use Illuminate\Support\ServiceProvider;

class NamedModelRoutesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
