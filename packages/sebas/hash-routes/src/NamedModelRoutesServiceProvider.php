<?php

namespace Sebas\HashRoutesProvider;

use Illuminate\Support\ServiceProvider;

class HashRoutesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
