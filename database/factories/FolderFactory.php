<?php

use Faker\Generator as Faker;

$factory->define(Artfamily\Folder::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'user_id' => 1
    ];
});
