<?php

use Faker\Generator as Faker;

$factory->define(Artfamily\Challenge::class, function (Faker $faker) {
    return [
        'caption' => $faker->words($faker->numberBetween(1, 5), true),
        'description' => $faker->sentences($faker->numberBetween(1, 5), true),
    ];
});
