<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

$factory->define(Artfamily\Image::class, function (Faker $faker) {
    $temp = $faker->image(null, $faker->numberBetween(400, 1000), $faker->numberBetween(400, 1000));

    $image = new Artfamily\Image();
    $image->setImage($temp, $save = false);
    return [
        'path' => $image->path,
        'thumbnail_path' => $image->thumbnail_path,
        'big_path' => $image->big_path,
        'aspect_ratio' => $image->aspect_ratio,
    ];
});
