<?php

use Faker\Generator as Faker;

$factory->define(Artfamily\Comment::class, function (Faker $faker) {
    return [
        'body' => $faker->sentences($faker->numberBetween(1, 5), true),
    ];
});
