<?php

use Faker\Generator as Faker;

$factory->define(Artfamily\Response::class, function (Faker $faker) {
    return [
        'description' => $faker->sentences($faker->numberBetween(1, 5), true),
    ];
});
