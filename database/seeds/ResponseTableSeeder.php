<?php

use Illuminate\Database\Seeder;

class ResponseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $users = Artfamily\User::all();
        foreach(Artfamily\Challenge::all() as $challenge) {
            factory(Artfamily\Response::class, 2)->make()->each(function($response) use ($users, $challenge) {
                $response->user()->associate($users->random());
                $response->challenge()->associate($challenge);
                $response->save();

                $image = factory(Artfamily\Image::class)->make();
                $image->imagable()->associate($response);
                $image->save();
            });
        }
    }
}
