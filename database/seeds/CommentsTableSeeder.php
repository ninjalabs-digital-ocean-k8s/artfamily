<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    public $commentables = [
        Artfamily\Creation::class,
        Artfamily\Sketch::class,
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->commentables as $commentableclass) {
            foreach($commentableclass::all() as $commentable) {
                $users = Artfamily\User::all()->pluck('id');
                foreach(range(0,5) as $index) {
                    $commentable->comments()->save(factory(Artfamily\Comment::class)->make([ 'user_id' => $users->random() ]));
                }
            }
        }
    }
}
