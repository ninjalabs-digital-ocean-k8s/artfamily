<?php

use Illuminate\Database\Seeder;
use Artfamily\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        factory(Artfamily\User::class, 5)->create()->each(function ($user) use ($faker) {
            $user->folders()->saveMany(factory(Artfamily\Folder::class, 2)->make());

            $user->creations()->saveMany(factory(Artfamily\Creation::class, 2)->make());
            $user->sketches()->saveMany(factory(Artfamily\Sketch::class, 2)->make());

            $user->creations()->each(function ($creation) use ($faker, $user) {
                $image = factory(Artfamily\Image::class)->make();
                $image->imagable()->associate($creation);
                $image->save();

                $creation->folders()->sync($faker->randomElements($user->folders->pluck('id')->all(), $faker->numberBetween(0, count($user->folders))));
            });

            $user->sketches()->each(function ($sketch) use ($faker, $user) {
                $image = factory(Artfamily\Image::class)->make();
                $image->imagable()->associate($sketch);
                $image->save();

                $sketch->folders()->sync($faker->randomElements($user->folders->pluck('id')->all(), $faker->numberBetween(0, count($user->folders))));
            });
        });

        Artfamily\User::all()[0]->fill([
            'name' => 'Testing Account',
            'email' => 'testing@local.machine',
            'password' => Hash::make('just_for_testing'),
        ])->save();

        Artfamily\User::all()[1]->fill([
            'name' => 'Sebastian Dittrich',
            'email' => 'sebastian.dittrich@live.de',
            'password' => Hash::make('hallohallo'),
        ])->save();
    }
}
