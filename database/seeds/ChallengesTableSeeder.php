<?php

use Illuminate\Database\Seeder;

class ChallengesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artfamily\User::all()->each(function($user) {
            $user->challenges()->saveMany(factory(Artfamily\Challenge::class, 2)->make());
        });
    }
}
