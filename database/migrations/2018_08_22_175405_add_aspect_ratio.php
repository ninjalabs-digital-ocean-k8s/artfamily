<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Storage;

class AddAspectRatio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->decimal('aspect_ratio');
        });

        Artfamily\Image::all()->each(function($image) {
            $image->path = 'images/' . $image->path;
            $image->aspect_ratio = $image->dimensions()->width / $image->dimensions()->height;
            $image->save();
        });

        Storage::put('images/placeholder.png', file_get_contents(public_path('pictures/image-placeholder.png')));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn('aspect_ratio');
        });

        Artfamily\Image::all()->each(function($image) {
            $image->path = substr($image->path, 6);
        });

        Storage::delete('images/placeholder.png');
    }
}
