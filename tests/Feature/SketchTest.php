<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SketchTest extends TestCase
{
    use Traits\ResourceAll;

    public $model = \Artfamily\Sketch::class;
}
