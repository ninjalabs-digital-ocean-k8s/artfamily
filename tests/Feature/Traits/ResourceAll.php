<?php

namespace Tests\Feature\Traits;

trait ResourceAll {

    use Resource, Index, Show, Create, Store, Edit, Update, Destroy;

}