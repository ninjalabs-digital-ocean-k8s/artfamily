<?php

namespace Tests\Feature\Traits;

trait withUser {

    public function user() {
        return \Artfamily\User::where('email', 'testing@local.machine')->first();
    }

}