<?php

namespace Tests\Feature\Traits;

trait Resource {
    use withUser;

    public function setUp() {
        parent::setUp();

        $this->staticmodel = $this->model;
        $this->model = $this->model::first();
    }

    public function create($class) {
        return factory($class)->create([ 'user_id' => $this->user()->id ]);
    }

}