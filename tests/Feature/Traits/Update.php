<?php

namespace Tests\Feature\Traits;

trait Update {

    public function testUpdate() {
        $this->put($this->model->route('update'))->assertStatus(403);
    }

}