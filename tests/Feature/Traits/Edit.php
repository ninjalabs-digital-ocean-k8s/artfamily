<?php

namespace Tests\Feature\Traits;

trait Edit {

    public function testEdit() {
        $user = $this->user();

        $this->get($this->model->route('edit'))->assertStatus(403);

        $this
            ->actingAs($user)
            ->get($this->staticmodel::where('user_id', '!=', $user->id)->first()->route('edit'))
            ->assertStatus(403);

        $this
            ->actingAs($user)
            ->get($user->hasMany($this->staticmodel)->first()->route('edit'))
            ->assertStatus(200);
    }

}