<?php

namespace Tests\Feature\Traits;

trait Destroy {

    public function testDestroy() {
        $model = $this->create($this->staticmodel);

        $this->delete($model->route('destroy'))->assertStatus(403);
        $this
            ->actingAs($this->user())
            ->delete($model->route('destroy'))
            ->assertRedirect();
    }

}