<?php

namespace Tests\Feature\Traits;

trait Create {

    public function testCreate() {
        $user = $this->user();

        $this->get($this->model->route('create'))->assertStatus(403);

        $this
            ->actingAs($user)
            ->get($this->model->route('create'))
            ->assertStatus(200);
    }

}