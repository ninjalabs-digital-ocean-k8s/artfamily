<?php

namespace Tests\Feature\Traits;

trait Show {

    public function testShow() {
        $this->get($this->model->route('show'))->assertStatus(200);
    }

}