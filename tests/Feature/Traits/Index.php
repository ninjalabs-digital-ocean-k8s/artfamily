<?php

namespace Tests\Feature\Traits;

trait Index {

    public function testIndex() {
        $this->get($this->model->route('index'))->assertStatus(200);
    }

}