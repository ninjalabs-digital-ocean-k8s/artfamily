<?php

namespace Tests\Feature\Traits;

trait Store {

    public function testStore() {
        $this->post($this->model->route('store'))->assertStatus(403);
    }

}