<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use Traits\Resource, Traits\Show;

    public $model = \Artfamily\User::class;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex() {
        $this->get('/users')->assertStatus(200);
    }
}
