<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    use Traits\Resource, Traits\Store, Traits\Destroy;

    public $model = \Artfamily\Comment::class;

    public function create() {
        return factory(\Artfamily\Comment::class)->create([
            'user_id' => $this->user()->id,
            'commentable_id' => \Artfamily\Creation::first()->id,
            'commentable_type' => \Artfamily\Creation::class,
        ]);
    }
}
