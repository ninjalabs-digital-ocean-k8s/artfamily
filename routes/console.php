<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Http\File;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('images:rebuild', function () {

    Artfamily\Image::all()->each(function($image) {
        if(Storage::exists($image->path)) {
            $temp = Storage::putFile('temp', new File(storage_path('app/public/' . $image->path)));

            $image->setImage(Storage::path($temp));
            Storage::delete($temp);
        }   
    });

    Storage::put('images/placeholder.png', file_get_contents(public_path('pictures/image-placeholder.png')));
    Storage::put('images/placeholder_thumbnail.png', file_get_contents(public_path('pictures/image-placeholder.png')));
    Storage::put('images/placeholder_big.png', file_get_contents(public_path('pictures/image-placeholder.png')));
    
})->describe('Rebuild thumbnails and big images');