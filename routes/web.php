<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([ 'verify' => true ]);

Route::redirect('/home', '/');

Route::get('/', 'HomeController@index')->name('home');

Route::resources([
    'creations' => 'CreationController',
    'sketches' => 'SketchController',
    'challenges' => 'ChallengeController',
]);

// Comments
Route::resource('comments', 'CommentsController')
    ->only([ 'store', 'destroy' ]);
Route::get('{commentable_type}/{commentable_id}#comment:{comment}')
    ->name('comments.show');

// Responses
Route::resource('responses', 'ResponseController')
    ->only([ 'show', 'store', 'edit', 'update', 'destroy' ]);
Route::get('challenges/{challenge}/responses/create', 'ResponseController@create')
    ->name('responses.create');

// Users
Route::resource('users', 'UserController')
    ->except([ 'show' ]);
Route::get('{user}/togglefollow', 'UserController@toggleFollow')
    ->middleware('auth')
    ->name('users.togglefollow');
Route::post('/validate-username', 'UserController@validateUsername')
    ->middleware('auth');
Route::get('/{user}/notifications', 'UserController@notifications')
    ->middleware('auth')
    ->name('users.notifications');

// Folders
Route::resource('folders', 'FolderController')
    ->except([ 'index' ]);

// Push Subscriptions
Route::put('push-subscription', 'PushSubscriptionController@update');
Route::delete('push-subscription', 'PushSubscriptionController@destroy');

// User
Route::get('/{user}', 'UserController@show')
    ->name('users.show');
