<?php

namespace Artfamily;

use Balping\HashSlug\HasHashSlug;

class Folder extends Model
{
    use hasRoutes, HasHashSlug;
    
    public static $icons = [ 'checkmark', 'image', 'heart', 'home', 'pencil' ];

    protected $fillable = [ 'name', 'icon', 'user_id' ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function creations()
    {
        return $this->morphedByMany(Creation::class, 'foldable');
    }

    public function sketches()
    {
        return $this->morphedByMany(Sketch::class, 'foldable');
    }

    public function images($limit = null) 
    {
        return $this->imagables()->map(function($item) {
            return $item->image;
        });
    }

    public function imagables() {
        $folder = $this;
        return Imagable::all()->filter(function($element) use ($folder) {
            return $element->folders->contains($folder);
        });
    }
}
