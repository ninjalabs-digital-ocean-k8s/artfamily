<?php

namespace Artfamily;

class Name {
    public $name;

    public function __construct(string $name) {
        $this->name = class_basename( $name );
    }

    public function lower() {
        $this->name = strtolower( $this->name );
        return $this;
    }
    public function upper() {
        $this->name = title_case( $this->name );
        return $this;
    }

    public function singular() {
        $this->name = str_singular( $this->name );
        return $this;
    }
    public function plural() {
        $this->name = str_plural( $this->name );
        return $this;
    }

    public function route() {
        return snake_case($this->plural()->get());
    }

    public function get() {
        return $this->name;
    }
}

trait hasNames
{   
    public static $hasNames = true;

    public function names() {
        return new Name(self::class);
    }
}
