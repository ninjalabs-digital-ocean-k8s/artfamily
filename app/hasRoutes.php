<?php

namespace Artfamily;

trait hasRoutes
{
    public function route(String $action = 'show', ...$args) {
        return route(self::routename() . '.' . $action, $this->routeParameters(), ...$args);
    }

    public function routeParameters() {
        return $this;
    }

    public function redirect(...$args) {
        return redirect($this->route(...$args));
    }

    public static function routename() {
        return str_plural(strtolower(class_basename(get_called_class())));
    }

    public static function routeIdName() {
        return str_singular(self::routename());
    }
}