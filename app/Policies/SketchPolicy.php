<?php

namespace Artfamily\Policies;

use Artfamily\User;
use Artfamily\Sketch;
use Illuminate\Auth\Access\HandlesAuthorization;

class SketchPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create sketches.
     *
     * @param  \Artfamily\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the sketch.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Sketch  $sketch
     * @return mixed
     */
    public function update(User $user, Sketch $sketch)
    {
        return $user->owns($sketch);
    }

    /**
     * Determine whether the user can delete the sketch.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Sketch  $sketch
     * @return mixed
     */
    public function delete(User $user, Sketch $sketch)
    {
        return $user->owns($sketch);
    }
}
