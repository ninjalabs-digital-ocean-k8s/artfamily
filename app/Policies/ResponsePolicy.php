<?php

namespace Artfamily\Policies;

use Artfamily\User;
use Artfamily\Response;
use Illuminate\Auth\Access\HandlesAuthorization;

class ResponsePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create responsees.
     *
     * @param  \Artfamily\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the response.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Response  $response
     * @return mixed
     */
    public function update(User $user, Response $response)
    {
        return $user->owns($response);
    }

    /**
     * Determine whether the user can delete the response.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Response  $response
     * @return mixed
     */
    public function delete(User $user, Response $response)
    {
        return $user->owns($response);
    }
}
