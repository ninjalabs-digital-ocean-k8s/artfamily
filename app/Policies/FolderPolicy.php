<?php

namespace Artfamily\Policies;

use Artfamily\User;
use Artfamily\Folder;
use Illuminate\Auth\Access\HandlesAuthorization;

class FolderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create folders.
     *
     * @param  \Artfamily\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the folder.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Folder  $folder
     * @return mixed
     */
    public function update(User $user, Folder $folder)
    {
        return $user->id === $folder->user_id;
    }

    /**
     * Determine whether the user can delete the folder.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Folder  $folder
     * @return mixed
     */
    public function delete(User $user, Folder $folder)
    {
        return $user->id === $folder->user_id;
    }

    public function insert(User $user, Folder $folder) {
        return $user->id === $folder->user_id;
    }
}
