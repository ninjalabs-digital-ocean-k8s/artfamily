<?php

namespace Artfamily\Policies;

use Artfamily\User;
use Artfamily\Challenge;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChallengePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the challenge.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Challenge  $challenge
     * @return mixed
     */
    public function view(User $user, Challenge $challenge)
    {
        return true;
    }

    /**
     * Determine whether the user can create challenges.
     *
     * @param  \Artfamily\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the challenge.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Challenge  $challenge
     * @return mixed
     */
    public function update(User $user, Challenge $challenge)
    {
        return $user->owns($challenge);
    }

    /**
     * Determine whether the user can delete the challenge.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Challenge  $challenge
     * @return mixed
     */
    public function delete(User $user, Challenge $challenge)
    {
        return $user->owns($challenge);
    }
}
