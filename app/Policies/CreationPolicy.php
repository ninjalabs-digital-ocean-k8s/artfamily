<?php

namespace Artfamily\Policies;

use Artfamily\User;
use Artfamily\Creation;
use Illuminate\Auth\Access\HandlesAuthorization;

class CreationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the creation.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Creation  $creation
     * @return mixed
     */
    public function view(User $user, Creation $creation)
    {
        return true;
    }

    /**
     * Determine whether the user can create creations.
     *
     * @param  \Artfamily\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the creation.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Creation  $creation
     * @return mixed
     */
    public function update(User $user, Creation $creation)
    {
        return $user->owns($creation);
    }

    /**
     * Determine whether the user can delete the creation.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Creation  $creation
     * @return mixed
     */
    public function delete(User $user, Creation $creation)
    {
        return $user->owns($creation);
    }
}
