<?php

namespace Artfamily\Policies;

use Artfamily\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the user.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\User  $user
     * @return mixed
     */
    public function update(User $user, User $otheruser)
    {
        return $user->id === $otheruser->id;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\User  $user
     * @return mixed
     */
    public function delete(User $user, User $otheruser)
    {
        return $user->id === $otheruser->id;
    }
}
