<?php

namespace Artfamily\Policies;

use Artfamily\User;
use Artfamily\Comment;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the comment.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Comment  $comment
     * @return mixed
     */
    public function view(User $user, Comment $comment)
    {
        return true;
    }

    /**
     * Determine whether the user can create comments.
     *
     * @param  \Artfamily\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the comment.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Comment  $comment
     * @return mixed
     */
    public function update(User $user, Comment $comment)
    {
        return $user->owns($comment);
    }

    /**
     * Determine whether the user can delete the comment.
     *
     * @param  \Artfamily\User  $user
     * @param  \Artfamily\Comment  $comment
     * @return mixed
     */
    public function delete(User $user, Comment $comment)
    {
        return $user->owns($comment);
    }
}
