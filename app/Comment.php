<?php

namespace Artfamily;

use Balping\HashSlug\HasHashSlug;

class Comment extends Model
{
    use hasHas, hasRoutes, HasHashSlug;

    protected $fillable = [ 'body', 'user_id' ];

    public function routeParameters() {
        return [
            'id' => $this->id,
            'commentable_type' => optional($this->commentable)->routename(),
            'commentable_id' => optional($this->commentable)->getRouteKey(),
        ];
    }

    public function commentable() {
        return $this->morphTo();
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
