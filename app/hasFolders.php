<?php

namespace Artfamily;

trait hasFolders
{
    public static $hasFolders = true;

    public function folders() {
        return $this->morphToMany(Folder::class, 'foldable');
    }
}
