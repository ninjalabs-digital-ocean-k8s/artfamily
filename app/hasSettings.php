<?php

namespace Artfamily;

use Grimthorr\LaravelUserSettings\Facade as Setting;

class Settings {
    public $id;

    public function __construct(int $id) {
        $this->id = $id;
    }
    
    public function save() {
        return Setting::save($this->id);
    }

    public function load() {
        return Setting::load($this->id);
    }

    public function set($key, $value) {
        return Setting::set($key, $value, $this->id);
    }

    public function get($key, $default = null) {
        if(!$default) {
            $default = config('laravel-user-settings.default.' . $key);
        }
        return Setting::get($key, $default, $this->id);
    }

    public function forget($key) {
        return Setting::forget($key, $this->id);
    }
    
    public function has($key) {
        return Setting::has($key, $this->id);
    }
}

trait hasSettings
{
    protected $settings;

    public function settings() {
        if(!isset($this->settings)) {
            $this->settings = new Settings($this->id);
        }
        return $this->settings;
    }
}