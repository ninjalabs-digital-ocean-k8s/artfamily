<?php

namespace Artfamily\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use Artfamily\Comment;

class NewComment extends Notification implements ShouldQueue
{
    use Queueable;

    public $comment;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channels('NewComment')->all();
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting($this->comment->user->name . ' has commented')
                    ->line($this->comment->user->name . ' commented: ' . $this->comment->body)
                    ->action('View Comment', $this->comment->commentable->route())
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the webpush representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\WebPushMessage
     */
    public function toWebPush($notifiable)
    {
        return (new WebPushMessage())
            ->title($this->comment->user->name . ' has commented')
            ->body($this->comment->user->name . ': ' . $this->comment->body)
            ->icon($this->comment->user->avatar()->url())
            ->badge(asset('/pictures/badge.png'))
            ->data([ 'link' => $this->comment->route() ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->comment->id,
        ];
    }
}
