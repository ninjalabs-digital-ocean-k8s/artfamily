<?php

namespace Artfamily\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use Artfamily\Challenge;

class NewChallenge extends Notification implements ShouldQueue
{
    use Queueable;

    public $challenge;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Challenge $challenge)
    {
        $this->challenge = $challenge;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channels('NewChallenge')->all();
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('A new Challenge by ' . $this->challenge->user->name)
                    ->line($this->challenge->user->name . ' has just started a new challenge:')
                    ->line($this->challenge->caption)
                    ->action('View Challenge', $this->challenge->route())
                    ->line('Be the first to solve it!')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the webpush representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\WebPushMessage
     */
    public function toWebPush($notifiable)
    {
        return (new WebPushMessage())
            ->title($this->challenge->caption)
            ->icon($this->challenge->user->avatar()->url())
            ->data([ 'link' => $this->challenge->route() ])
            ->body($this->challenge->user->name . ' has just started a new challenge!')
            ->badge(asset('/pictures/badge.png'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->challenge->id,
        ];
    }
}
