<?php

namespace Artfamily\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use Artfamily\Sketch;

class NewSketch extends Notification implements ShouldQueue
{
    use Queueable;

    public $sketch;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Sketch $sketch)
    {
        $this->sketch = $sketch;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channels('NewSketch')->all();
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('A new Sketch!')
                    ->line($this->sketch->user->name . ' has just uploaded a new sketch!')
                    ->action('View Sketch', $this->sketch->route())
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the webpush representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\WebPushMessage
     */
    public function toWebPush($notifiable)
    {
        return (new WebPushMessage())
            ->title('New Sketch!')
            ->body($this->sketch->user->name . ' has just uploaded a new sketch!')
            ->icon($this->sketch->user->avatar()->url())
            ->image($this->sketch->image->url())
            ->badge(asset('/pictures/badge.png'))
            ->data([ 'link' => $this->sketch->route() ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->sketch->id,
        ];
    }
}
