<?php

namespace Artfamily\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use Artfamily\Creation;

class NewCreation extends Notification implements ShouldQueue
{
    use Queueable;

    public $creation;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Creation $creation)
    {
        $this->creation = $creation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channels('NewCreation')->all();
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('A new Creation!')
                    ->line($this->creation->user->name . ' has just uploaded a new creation!')
                    ->action('View Creation', $this->creation->route())
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the webpush representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\WebPushMessage
     */
    public function toWebPush($notifiable)
    {
        return (new WebPushMessage())
            ->title('New Creation!')
            ->body($this->creation->user->name . ' has just uploaded a new creation!')
            ->icon($this->creation->user->avatar()->url())
            ->image($this->creation->image->url())
            ->badge(asset('/pictures/badge.png'))
            ->data([ 'link' => $this->creation->route() ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->creation->id,
        ];
    }
}
