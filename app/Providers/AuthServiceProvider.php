<?php

namespace Artfamily\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Artfamily\Folder' => 'Artfamily\Policies\FolderPolicy',
        \Artfamily\Creation::class => \Artfamily\Policies\CreationPolicy::class,
        \Artfamily\Sketch::class => \Artfamily\Policies\SketchPolicy::class,
        \Artfamily\Challenge::class => \Artfamily\Policies\ChallengePolicy::class,
        \Artfamily\Comment::class => \Artfamily\Policies\CommentPolicy::class,
        \Artfamily\Response::class => \Artfamily\Policies\ResponsePolicy::class,
        \Artfamily\User::class => \Artfamily\Policies\UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
