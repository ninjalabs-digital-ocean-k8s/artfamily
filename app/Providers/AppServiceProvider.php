<?php

namespace Artfamily\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Laravel\Horizon\Horizon;
use Artfamily\Creation;
use Artfamily\Sketch;
use Artfamily\Challenge;
use Artfamily\Comment;
use Artfamily\Observers\CreationObserver;
use Artfamily\Observers\SketchObserver;
use Artfamily\Observers\ChallengeObserver;
use Artfamily\Observers\CommentObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Set maximum varchar length for utf8mb4
        Schema::defaultStringLength(191);

        Creation::observe(CreationObserver::class);
        Sketch::observe(SketchObserver::class);
        Challenge::observe(ChallengeObserver::class);
        Comment::observe(CommentObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
