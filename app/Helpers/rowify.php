<?php

if (!function_exists('rowify')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function rowify($array, Int $width)
    {
        $matrix = [[]];
        $count = 0;

        foreach($array as $item) {
            if(!($count < $width)) {
                $matrix[] = [];
                $count = 0;
            }

            $matrix[count($matrix) - 1][] = $item;
            $count++;
        }
        
        return $matrix;
    }
}
