<?php

namespace Artfamily;

trait hasHas {
    public static function has($name) {
        $prop = 'has' . studly_case($name);
        return isset(self::$$prop) ? self::$$prop : false;
    }
}