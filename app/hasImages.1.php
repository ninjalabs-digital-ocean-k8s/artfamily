<?php

namespace Artfamily;

trait hasImages
{   
    public static $hasImages = true;

    public function images() {
        return $this->morphMany(Image::class, 'imagable')->withDefault();
    }
}
