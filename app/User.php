<?php

namespace Artfamily;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Rennokki\Befriended\Traits\Follow;
use Rennokki\Befriended\Contracts\Following;
use NotificationChannels\WebPush\HasPushSubscriptions;
use NotificationChannels\WebPush\WebPushChannel;

class User extends Authenticatable implements Following, MustVerifyEmail
{
    use Notifiable, hasRoutes, Follow, hasImage, HasPushSubscriptions, hasSettings;

    protected static $alphabet = '$()[]{}0123456789';
    protected static $minSlugLength = 10;

    public static $restrictions = [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed',
        'username' => 'required|string|unique:users|max:255|alpha_dash'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function avatar() {
        $avatar = $this->image;
        if($avatar['isDefault']) {
            $avatar->path = '/images/avatar.png';
            $avatar->big_path = '/images/avatar.png';
            $avatar->thumbnail_path = '/images/avatar.png';
            $avatar->aspect_ratio = 1/1;
        }
        return $avatar;
    }

    public function cover() {
        if($this->imagables()->count() > 0) {
            return $this->imagables()->first()->image->url();
        }
        return asset('pictures/cover.jpg');
    }

    public function creations() 
    {
        return $this->hasMany(Creation::class);
    }

    public function sketches() 
    {
        return $this->hasMany(Sketch::class);
    }

    public function challenges() {
        return $this->hasMany(Challenge::class);
    }

    public function folders()
    {
        return $this->hasMany(Folder::class);
    }

    public function images() 
    {
        return Image::all()->filter(function($element) {
            return $this->owns($element->imagable);
        });
    }

    public function imagables() {
        $user = $this;
        return Imagable::query(function() use ($user) {
            return $this->where('user_id', $user->id);
        });
    }

    public function isLoggedIn() {
        return auth()->id() === $this->id;
    }

    public function owns($model) {
        return $model->user_id === $this->id;
    }

    public function getRouteKey() {
        return '@' . $this->username;
    }

    public function channels($for = null) {
        if($for) {
            $wanttoreceive = $this->settings()->get('notifications.types.' . $for);
            if($wanttoreceive === false) {
                return collect([]);
            }
        }

        return collect($this->settings()->get('notifications.channels'))
                ->filter(function($channel) { return $channel; })
                ->keys()
                ->concat([ 'database', WebPushChannel::class ])
                ->unique();
    }
}
