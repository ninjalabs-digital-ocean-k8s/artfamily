<?php

namespace Artfamily;

use Balping\HashSlug\HasHashSlug;

class Response extends Model
{
    use hasHas, hasImage, hasRoutes, HasHashSlug, hasCaption, hasComments;

    public static $hasCaptionLink = true;
    public static $hasComputedCaption = true;

    protected $fillable = [ 'description', 'user_id', 'challenge_id' ];

    public function getCaptionAttribute() {
        return $this->challenge->caption;
    }

    public function getCaptionLinkAttribute() {
        return $this->challenge->route();
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function challenge() {
        return $this->belongsTo(Challenge::class);
    }
}
