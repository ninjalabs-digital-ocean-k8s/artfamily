<?php

namespace Artfamily;

trait hasComments
{
    public static $hasComments = true;

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function commentsSorted() {
        return $this->comments()->orderBy('updated_at', 'DESC')->get();
    }
}
