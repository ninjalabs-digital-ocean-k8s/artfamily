<?php

namespace Artfamily;

class Imagable {
    protected static $models = [
        Creation::class,
        Sketch::class,
    ];

    public static function all() {
        return self::get();
    }

    public static function query($query) {
        return self::get([ 'query' => $query ]);
    }

    public static function get($config = []) {
        $ret = collect();
        foreach(self::$models as $model) {
            $elements = $model::all();
            if(array_key_exists('query', $config)) {
                $elements = $config['query']->call($elements);
            }
            $ret = $ret->concat($elements);
        }
        return $ret;
    }
}