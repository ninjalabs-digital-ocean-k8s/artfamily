<?php

namespace Artfamily;

use Artfamily\User;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{    
    protected $supported = [ 'PNG', 'JPEG', 'GIF', 'webP' ];

    public static $default = [
        'path' => 'images/placeholder.png',
        'thumbnail_path' => 'images/placeholder.png',
        'big_path' => 'images/placeholder.png',
        'aspect_ratio' => 1108 / 771,
        'isDefault' => true
    ];

    public static $restrictions = 'image|mimes:png,jpg,jpeg,bmp';

    public function imagable() {
        return $this->morphTo();
    }

    public function full()
    {
        return Storage::url($this->path);
    }

    public function big()
    {
        return Storage::url($this->big_path);
    }

    public function url() {
        return Storage::url($this->thumbnail_path);
    }

    public function getImage() {
        return \IImage::make(Storage::get($this->path));
    }

    public function dimensions() {
        $array = getimagesize(public_path('/storage/' . $this->path));
        return (object) [
            'width' => $array[0],
            'height' => $array[1],
        ];
    }

    public function setImage($image, $save = true, $square = false)
    {
        // If String is given, turn it into a File
        if(gettype($image) == 'string') {
            $image = new File($image);
        }

        // Delete old file
        if($this->path) {
            Storage::delete($this->path);
        }
        if($this->thumbnail_path) {
            Storage::delete($this->thumbnail_path);
        }
        if($this->big_path) {
            Storage::delete($this->big_path);
        }

        // Save new file
        $this->path = Storage::putFile('images', $image);
        $this->big_path = Storage::putFile('bigs', $image);
        $this->thumbnail_path = Storage::putFile('thumbnails', $image);

        $iimage = $this->getImage();
        if($square && $iimage->height() != $iimage->width()) {
            $height = $iimage->height();
            $width = $iimage->width();
            $shorter = $height < $width ? $height : $width;
            $iimage->crop($shorter, $shorter);
            Storage::put($this->path, $iimage->encode());
        }

        $iimage->resize(1920, null, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        Storage::put($this->big_path, $iimage->encode());
        
        $iimage->resize(500, null, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        Storage::put($this->thumbnail_path, $iimage->encode());
        
        // Create intervention image
        $dimensions = $this->dimensions();
        // Save aspect ratio
        $this->aspect_ratio = $dimensions->width / $dimensions->height;

        // Save if it should save
        if($save) {
            $this->save();
        }

        return $this;
    }
}
