<?php

namespace Artfamily;

use Balping\HashSlug\HasHashSlug;

class Sketch extends Model
{
    use hasImage, hasFolders, hasComments, hasRoutes, hasHas, hasCaption, HasHashSlug, hasNames;

    protected $fillable = [ 'path', 'caption', 'description', 'user_id', 'folder_id' ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
