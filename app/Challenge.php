<?php

namespace Artfamily;

use Balping\HashSlug\HasHashSlug;

class Challenge extends Model
{
    use hasRoutes, hasHas, hasCaption, HasHashSlug, hasNames;

    protected $fillable = [ 'path', 'caption', 'description', 'user_id', 'folder_id' ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function responses() {
        return $this->hasMany(Response::class);
    }

    public function responsesSorted() {
        return $this->responses()->orderBy('updated_at', 'DESC')->get();
    }
}
