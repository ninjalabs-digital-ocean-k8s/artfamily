<?php

namespace Artfamily\Http\Controllers;

use Illuminate\Http\Request;
use Artfamily\Notifications\PushNotificationRequest;

class PushSubscriptionController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'endpoint' => 'required'
        ]);

        $request->user()->updatePushSubscription(
            $request->endpoint,
            $request->key,
            $request->token
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $this->validate(request(), ['endpoint' => 'required']);
        request()->user()->deletePushSubscription(request()->endpoint);
        return response()->json(null, 204);
    }
}
