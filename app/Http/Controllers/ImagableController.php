<?php

namespace Artfamily\Http\Controllers;

use Artfamily\Creation;
use Artfamily\Image;
use Illuminate\Http\Request;
use Artfamily\Folder;

class ImagableController extends Controller
{
    public function viewName() {
        return 'imagables';
    }

    public function view($name, $data) {
        return view()->first([$this->viewName() . '.' . $name, 'imagables.' . $name], $data)->with('model', $this->model());
    }

    public function model() {
        return (object) [
            'name' => strtolower( str_singular( class_basename( $this->model ) ) ),
            'Name' => title_case( str_singular( class_basename( $this->model ) ) ),
            'names' => strtolower( str_plural( class_basename( $this->model ) ) ),
            'Names' => title_case( str_plural( class_basename( $this->model ) ) ),
            'route' => snake_case( str_plural( class_basename( $this->model ) ) ),
            'static' => $this->model,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->view('index', [
            'imagables' => $this->model::latest()->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', $this->model);

        $belonging = $this->getBelonging();

        $data = [ 
            'imagable' => null,
        ];
        
        if($belonging->belongs) {
            $data['belonging'] = (object) [
                'key' => $belonging->key,
                'value' => $request->route($belonging->route)->id,
            ];
        }

        return $this->view('create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  $imagable
     * @return \Illuminate\Http\Response
     */
    public function show($imagable)
    {
        return $this->view('show', [
            'imagable' => $imagable,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->model);

        $belonging = $this->getBelonging();

        $rules = [
            'caption' => ($this->model::has('caption') && !$this->model::has('ComputedCaption') ? 'required' : 'nullable') . '|string|max:255',
            'description' => 'nullable|string',
            'folders' => 'nullable|string',
            'image' => ($this->model::has('image') && $this->isImageRequired() ? 'required|' : '') . Image::$restrictions
        ];

        if($belonging->belongs) {
            $rules[$belonging->key] = 'required|integer|exists:' . $belonging->tablename . ',id';
        }

        $request->validate($rules);

        $imagable = new $this->model([
            'user_id' => auth()->id(),
            'caption' => request('caption'),
            'description' => request('description'),
        ]);
        if($belonging->belongs) {
            $imagable->{$belonging->key} = request($belonging->key);
        }
        $this->callIfExists('beforeImagableSave', $imagable);
        $imagable->save();

        if($this->model::has('folders')) {
            $imagable->folders()->sync($this->foldersFromString($request->input('folders')));
        }

        if($this->model::has('image') && $request->has('image')) {
            $image = new Image();
            $image->imagable()->associate($imagable);
            $image->setImage($request->file('image'));
            $image->save();
        }

        return $this->redirectStore($imagable);
    }

    public function redirectStore($imagable) {
        return redirect()->route($this->model()->route . '.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $imagable
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $imagable)
    {
        $this->authorize('update', $imagable);

        $belonging = $this->getBelonging();

        $data = [ 
            'imagable' => $imagable,
        ];
        
        if($belonging->belongs) {
            $data['belonging'] = (object) [
                'key' => $belonging->key,
                'value' => $imagable->{$belonging->key},
            ];
        }

        return $this->view('create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $imagable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $imagable)
    {
        $this->authorize('update', $imagable);

        $belonging = $this->getBelonging();

        $rules = [
            'caption' => ($this->model::has('caption') && !$this->model::has('ComputedCaption') ? 'required' : 'nullable') . '|string|max:255',
            'description' => 'nullable|string',
            'folders' => 'nullable|string',
            'image' => 'nullable|' . Image::$restrictions
        ];

        if($belonging->belongs) {
            $rules[$belonging->key] = 'required|integer|exists:' . $belonging->tablename . ',id';
        }
        
        $validated = $request->validate($rules);

        if($this->model::has('image') && $request->file('image')) {
            $imagable->image->setImage($request->file('image'));
        }

        $imagable->fill($request->all());
        
        $imagable->save();

        if($this->model::has('folders')) {
            $imagable->folders()->sync($this->foldersFromString($request->input('folders')));
        }

        return redirect()->route($this->model()->route . '.show', compact('imagable'));
    }

    public static function foldersFromString($string) {
        if(!strlen($string)) {
            return [];
        }
        $folders = explode(',', $string);
        foreach($folders as $folder) {
            $folder = intval($folder);
        }
        return $folders;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $imagable
     * @return \Illuminate\Http\Response
     */
    public function destroy($imagable)
    {
        $this->authorize('delete', $imagable);

        if($this->model::has('image')) {
            $imagable->image->delete();
        }
        $imagable->delete();
        return $this->redirectDestroy();
    }

    public function redirectDestroy() {
        return redirect()->route($this->model()->route . '.index');
    }

    public function isImageRequired() {
        return true;
    }

    public function getBelonging() {
        $belonging = $this->callIfExists('belonging');
        $belonging_key = snake_case(str_singular(class_basename($belonging))) . '_id';
        $belonging_db_name = snake_case(str_plural(class_basename($belonging)));

        return (object) [
            'belongs' => $belonging  ? true : false,
            'key' => $belonging_key,
            'tablename' => $belonging_db_name,
            'route' => snake_case(str_singular(class_basename($belonging))),
        ];
    }

    public function callIfExists($name, ...$args) {
        if(method_exists($this, $name)) {
            return $this->$name(...$args);
        }
        return null;
    }
}
