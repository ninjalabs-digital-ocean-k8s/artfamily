<?php

namespace Artfamily\Http\Controllers;

use Artfamily\Sketch;
use Illuminate\Http\Request;

class SketchController extends ImagableController
{
    use Imagable\Stores;
    
    public $model = Sketch::class;
}
