<?php

namespace Artfamily\Http\Controllers;

use Illuminate\Http\Request;
use Artfamily\Imagable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imagables = collect();
        $followsnoone = false;
        if(auth()->check()) {
            foreach(auth()->user()->following as $user) {
                $imagables = $imagables->concat($user->imagables());
            }

            if(auth()->user()->following->count() < 1) {
                $followsnoone = true;
            }
        } else {
            $imagables = Imagable::all();
        }
        return view('home', [
            'imagables' => $imagables,
            'followsnoone' => $followsnoone,
        ]);
    }
}
