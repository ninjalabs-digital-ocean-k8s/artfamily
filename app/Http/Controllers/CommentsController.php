<?php

namespace Artfamily\Http\Controllers;

use Artfamily\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Comment::class);
        $this->validate($request, [
            'commentable_type' => [ 'required', 'string', function($attribute, $value, $fail) {
                if(!$value::has('comments')) {
                    return $fail('This type of element is not commentable.');
                }
            } ],
            'commentable_id' => 'required|integer',
            'body' => 'required|string',
        ]);

        $comment = new Comment([ 'body' => $request->input('body') ]);
        $comment->commentable_type = $request->commentable_type;
        $comment->commentable_id = $request->commentable_id;

        if(!$comment->commentable) {
            return redirect()->back()->withErrors(['This element does not exist.'])->withInput();
        }

        $comment->user_id = auth()->id();
        $comment->save();
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Artfamily\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Artfamily\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);

        $comment->delete();
        
        return redirect()->back();
    }
}
