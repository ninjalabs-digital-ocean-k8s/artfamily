<?php

namespace Artfamily\Http\Controllers;

use Artfamily\Creation;
use Artfamily\Image;
use Illuminate\Http\Request;
use Artfamily\Folder;

class CreationController extends ImagableController
{
    public $model = Creation::class;
}
