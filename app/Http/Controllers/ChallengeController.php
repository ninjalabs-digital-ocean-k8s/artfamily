<?php

namespace Artfamily\Http\Controllers;

class ChallengeController extends ImagableController
{
    use Imagable\Stores;
    
    public $model = \Artfamily\Challenge::class;

    public function isImageRequired() {
        return false;
    }
    public function viewName() {
        return 'challenges';
    }

    public function beforeImagebleSave($imagable) {
        $imagable->challenge_id = intval(request('challenge_id'));
    }
}
