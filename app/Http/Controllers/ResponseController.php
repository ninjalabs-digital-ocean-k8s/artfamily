<?php

namespace Artfamily\Http\Controllers;

use Artfamily\Response;
use Artfamily\Challenge;
use Illuminate\Http\Request;
use Artfamily\Http\Requests\StoreResponse;

class ResponseController extends ImagableController
{
    public $model = Response::class;

    public function belonging() {
        return Challenge::class;
    }

    public function redirectStore($response) {
        return $response->challenge->redirect();
    }

    public function redirectDestroy() {
        return redirect()->back();
    }

    public function viewName() {
        return 'responses';
    }
    // public function store(StoreResponse $request) {
    //     // return var_dump($request->images);
    //     $validated = $request->validated();
    //     $validated['user_id'] = auth()->id();

    //     $response = Response::create($validated);

    //     $image = new Image();
    //     $image->imagable()->associate($response);
    //     $image->setImage($request->file('image'));
    //     $image->save();
    // }
}
