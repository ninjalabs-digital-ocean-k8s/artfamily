<?php

namespace Artfamily\Http\Controllers;

use Artfamily\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Artfamily\Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index', [
            'users' => User::orderBy('name')->get()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Artfamily\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', [ 'user' => $user ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Artfamily\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return view('users.edit', [ 'user' => $user ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Artfamily\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        // Leave out identical properties
        $except = ['password'];
        if($request->input('username') == $user->username) {
            $except[] = 'username';
        }
        if($request->input('email') == $user->email) {
            $except[] = 'email';
        }
        $rules = array_collapse([User::$restrictions, [ 'avatar' => 'nullable|' . Image::$restrictions ]]);
        $rules = array_except( $rules, $except );

        $validated = $request->validate( $rules );

        $user->fill( array_except( $request->all(), $except ) );

        // Update avatar if is given
        if($request->file('avatar')) {
            $avatar = new Image();
            $avatar->setImage($request->file('avatar'), $save = false, $square = true);

            if(!$user->avatar()->isDefault) {
                $user->avatar()->delete();
            }
            $avatar->imagable()->associate($user);
            $avatar->save();            
        }

        if(!!$request->get('notifications_mail_active') != $user->settings()->get('notifications.channels.mail')) {
            $user->settings()->set('notifications.channels.mail', !!$request->get('notifications_mail_active'));
        }

        collect($user->settings()->get('notifications.types'))->each(function($current, $name) use ($request, $user) {
            $new = !!$request->get('notifications_type_' . $name);
            if($new != $current) {
                $user->settings()->set('notifications.types.' . $name, $new);
            }
        });

        $user->settings()->save();
        $user->save();

        return $user->redirect();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Artfamily\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // 
    }

    public function notifications(User $user) {
        dispatch(function() use ($user) {
            $user->unreadNotifications->markAsRead();
        });
        return view('users.notifications', [ 'notifications' => $user->notifications ]);
    }

    public function toggleFollow(User $user) {
        if(auth()->user()->follows($user)) {
            auth()->user()->unfollow($user);
        } else {
            auth()->user()->follow($user);
        }

        return redirect()->back();
    }

    public function validateUsername() {
        $username = request('username');

        $validated = Validator::make(['username' => $username], [
            'username' => User::$restrictions['username'],
        ]);

        $errors = $validated->errors()->all();

        $user = new User();
        $user->username = $username;

        return [
            'valid' => count($errors) < 1 || auth()->user()->username == $user->username,
            'errors' => $errors,
            'url' => $user->route(),
        ];
    }
}
