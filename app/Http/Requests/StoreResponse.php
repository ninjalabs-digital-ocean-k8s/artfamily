<?php

namespace Artfamily\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreResponse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', \Artfamily\Response::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'description' => 'nullable|string',
            'challenge_id' => 'required|integer|exists:challenges,id',
        ];
        $images = count($this->input('image'));
        foreach(range(0, $images) as $index) {
            $rules['image.' . $index] = 'image|mimes:png,jpg,jpeg,bmp';
        }
        return $rules;
    }
}
