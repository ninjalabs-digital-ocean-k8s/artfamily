<?php

namespace Artfamily\Observers;

use Artfamily\Comment;
use Artfamily\Notifications\NewComment;

class CommentObserver
{
    /**
     * Handle the comment "created" event.
     *
     * @param  \Artfamily\Comment  $comment
     * @return void
     */
    public function created(Comment $comment)
    {
        $comment->commentable->user->notify(new NewComment($comment));
    }

    /**
     * Handle the comment "updated" event.
     *
     * @param  \Artfamily\Comment  $comment
     * @return void
     */
    public function updated(Comment $comment)
    {
        //
    }

    /**
     * Handle the comment "deleted" event.
     *
     * @param  \Artfamily\Comment  $comment
     * @return void
     */
    public function deleted(Comment $comment)
    {
        //
    }

    /**
     * Handle the comment "restored" event.
     *
     * @param  \Artfamily\Comment  $comment
     * @return void
     */
    public function restored(Comment $comment)
    {
        //
    }

    /**
     * Handle the comment "force deleted" event.
     *
     * @param  \Artfamily\Comment  $comment
     * @return void
     */
    public function forceDeleted(Comment $comment)
    {
        //
    }
}
