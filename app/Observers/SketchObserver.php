<?php

namespace Artfamily\Observers;

use Artfamily\Sketch;
use Artfamily\Notifications\NewSketch;

class SketchObserver
{
    /**
     * Handle the sketch "created" event.
     *
     * @param  \Artfamily\Sketch  $sketch
     * @return void
     */
    public function created(Sketch $sketch)
    {
        \Notification::send($sketch->user->followers, new NewSketch($sketch));
    }

    /**
     * Handle the sketch "updated" event.
     *
     * @param  \Artfamily\Sketch  $sketch
     * @return void
     */
    public function updated(Sketch $sketch)
    {
        //
    }

    /**
     * Handle the sketch "deleted" event.
     *
     * @param  \Artfamily\Sketch  $sketch
     * @return void
     */
    public function deleted(Sketch $sketch)
    {
        //
    }

    /**
     * Handle the sketch "restored" event.
     *
     * @param  \Artfamily\Sketch  $sketch
     * @return void
     */
    public function restored(Sketch $sketch)
    {
        //
    }

    /**
     * Handle the sketch "force deleted" event.
     *
     * @param  \Artfamily\Sketch  $sketch
     * @return void
     */
    public function forceDeleted(Sketch $sketch)
    {
        //
    }
}
