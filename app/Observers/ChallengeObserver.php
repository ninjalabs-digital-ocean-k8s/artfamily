<?php

namespace Artfamily\Observers;

use Artfamily\Challenge;
use Artfamily\Notifications\NewChallenge;

class ChallengeObserver
{
    /**
     * Handle the challenge "created" event.
     *
     * @param  \Artfamily\Challenge  $challenge
     * @return void
     */
    public function created(Challenge $challenge)
    {
        \Notification::send($challenge->user->followers, new NewChallenge($challenge));
    }

    /**
     * Handle the challenge "updated" event.
     *
     * @param  \Artfamily\Challenge  $challenge
     * @return void
     */
    public function updated(Challenge $challenge)
    {
        //
    }

    /**
     * Handle the challenge "deleted" event.
     *
     * @param  \Artfamily\Challenge  $challenge
     * @return void
     */
    public function deleted(Challenge $challenge)
    {
        //
    }

    /**
     * Handle the challenge "restored" event.
     *
     * @param  \Artfamily\Challenge  $challenge
     * @return void
     */
    public function restored(Challenge $challenge)
    {
        //
    }

    /**
     * Handle the challenge "force deleted" event.
     *
     * @param  \Artfamily\Challenge  $challenge
     * @return void
     */
    public function forceDeleted(Challenge $challenge)
    {
        //
    }
}
