<?php

namespace Artfamily\Observers;

use Artfamily\Creation;
use Artfamily\Notifications\NewCreation;

class CreationObserver
{
    /**
     * Handle the creation "created" event.
     *
     * @param  \Artfamily\Creation  $creation
     * @return void
     */
    public function created(Creation $creation)
    {
        \Notification::send($creation->user->followers, new NewCreation($creation));
    }

    /**
     * Handle the creation "updated" event.
     *
     * @param  \Artfamily\Creation  $creation
     * @return void
     */
    public function updated(Creation $creation)
    {
        //
    }

    /**
     * Handle the creation "deleted" event.
     *
     * @param  \Artfamily\Creation  $creation
     * @return void
     */
    public function deleted(Creation $creation)
    {
        //
    }

    /**
     * Handle the creation "restored" event.
     *
     * @param  \Artfamily\Creation  $creation
     * @return void
     */
    public function restored(Creation $creation)
    {
        //
    }

    /**
     * Handle the creation "force deleted" event.
     *
     * @param  \Artfamily\Creation  $creation
     * @return void
     */
    public function forceDeleted(Creation $creation)
    {
        //
    }
}
