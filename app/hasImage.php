<?php

namespace Artfamily;

trait hasImage
{   
    public static $hasImage = true;

    public function image() {
        return $this->morphOne(Image::class, 'imagable')->withDefault(Image::$default);
    }
}
